<?php
App::uses('AppController', 'Controller');
/**
 * Orders Controller
 *
 * @property Order $Order
 */
class OrdersController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Order->recursive = 0;
		$this->set('orders', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Order->id = $id;
		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		$this->set('order', $this->Order->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Order->create();
			if ($this->Order->save($this->request->data)) {
				$this->Session->setFlash(__('The order has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order could not be saved. Please, try again.'));
			}
		}
		$users = $this->Order->User->find('list');
		$restaurants = $this->Order->Restaurant->find('list');
		$this->set(compact('users', 'restaurants'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Order->id = $id;
		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Order->save($this->request->data)) {
				$this->Session->setFlash(__('The order has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Order->read(null, $id);
		}
		$users = $this->Order->User->find('list');
		$restaurants = $this->Order->Restaurant->find('list');
		$this->set(compact('users', 'restaurants'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Order->id = $id;
		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		if ($this->Order->delete()) {
			$this->Session->setFlash(__('Order deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Order was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Order->recursive = 0;
		$this->set('orders', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Order->id = $id;
		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		$this->set('order', $this->Order->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Order->create();
			if ($this->Order->save($this->request->data)) {
				$this->Session->setFlash(__('The order has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order could not be saved. Please, try again.'));
			}
		}
		$users = $this->Order->User->find('list');
		$restaurants = $this->Order->Restaurant->find('list');
		$this->set(compact('users', 'restaurants'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Order->id = $id;
		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Order->save($this->request->data)) {
				$this->Session->setFlash(__('The order has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Order->read(null, $id);
		}
		$users = $this->Order->User->find('list');
		$restaurants = $this->Order->Restaurant->find('list');
		$this->set(compact('users', 'restaurants'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Order->id = $id;
		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		if ($this->Order->delete()) {
			$this->Session->setFlash(__('Order deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Order was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function add_to_cart($product_id){
		$this->loadModel('Product');
		$this->Product->recursive = -1;
		$product = $this->Product->read(null, $product_id);
		$this->Cart->add($product, 1);
		$this->Session->setFlash('Produto incluído!');
		$this->redirect($_SERVER['HTTP_REFERER']);
	}

	public function remove_from_cart($product_id){
		$this->Cart->remove($product_id);
	}

	public function cart(){

	}

	public function empty_cart(){
		$this->Session->delete('Cart');
		$this->redirect('/');
	}

	public function add_details(){
		if($this->request->is('post')){
			if( $this->Session->read('CartDetails') ){
				$details = $this->Session->read('CartDetails');
			}else{
				$details = array();
			}

			$details['address'] = $this->request->data['address'];
			$details['payment'] = $this->request->data['payment'];
			$this->Session->write('CartDetails', $details);
			$this->redirect(array('controller' => 'orders', 'action' => 'finish'));
		}
	}

	public function finish(){
		if( !$this->Session->read('Cart') ){
			$this->redirect(array('controller' => 'orders', 'action' => 'cart'));
		
		}elseif( !$this->Session->read('CartDetails') ){
			$this->redirect(array('controller' => 'orders', 'action' => 'add_details'));
		
		}else{
			$this->loadModel('Restaurant');
			$user_id = $this->Auth->user('User.id');
			$cart = $this->Session->read('Cart');
			$restaurant = $this->Restaurant->getByProduct($cart['Product'][0]['id']);
			$total = 0;
			
			foreach($cart as $item){
				$total += $item['Product']['price'] * $item['qty'];
			}

			$orderData = array('Order' => array(
				'user_id' => $user_id,
				'restaurant_id' => $restaurant['Restaurant']['id'],
				'total' => $total,
				'comment' => 'Endereço: '.$this->Session->read('CartDetails.address').'<br>Pagamento: '.$this->Session->read('CartDetails.payment'),
				'status' => 1
				));

			$this->Order->save($orderData);

			$data = array('OrderProduct' => array());

			foreach($cart as $item){
				$data['OrderProduct'][] = array(
					'product_id' => $item['Product']['id'],
					'order_id' => $this->Order->id,
					'quantity' => $item['qty'],
					'price' => $item['Product']['price']
					);
			}

			$this->Order->OrderProduct->saveAll($data['OrderProduct']);
			
			$order = $this->Order->read(null, $this->Order->id);
			$this->_shareOrder($order);
			$this->_saveActivity($order);
			$this->Session->delete('CartDetails');
			$this->Session->delete('Cart');
		}
	}

	public function _shareOrder($order){
		$message = sprintf(
			'Fiz um pedido no delivery do restaurante "%s".',
			$order['Restaurant']['name']
			);

		$auth = $this->HybridAuth->getInstance();
		$adapter = $auth->getAdapter( $this->Session->read('current_provider') );

		$this->HybridAuth->postOnWall($adapter, array(
			'message' => $message,
			'link' => 'http://restauranteweb.com.br'
		));
	}

	public function _saveActivity($order){
		$this->loadModel('Activity');
		$this->Activity->save(array(
			'user_id' => $order['Order']['user_id'],
			'action' => 'fez um pedido em',
			'target_type' => 'Restaurant',
			'target_id' => $order['Order']['restaurant_id']
		));
	}
}
