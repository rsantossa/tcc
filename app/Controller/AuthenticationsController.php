<?php
App::uses('AppController', 'Controller');
/**
 * Authentications Controller
 *
 * @property Authentication $Authentication
 */
class AuthenticationsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Authentication->recursive = 0;
		$this->set('authentications', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Authentication->id = $id;
		if (!$this->Authentication->exists()) {
			throw new NotFoundException(__('Invalid authentication'));
		}
		$this->set('authentication', $this->Authentication->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Authentication->create();
			if ($this->Authentication->save($this->request->data)) {
				$this->Session->setFlash(__('The authentication has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The authentication could not be saved. Please, try again.'));
			}
		}
		$users = $this->Authentication->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Authentication->id = $id;
		if (!$this->Authentication->exists()) {
			throw new NotFoundException(__('Invalid authentication'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Authentication->save($this->request->data)) {
				$this->Session->setFlash(__('The authentication has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The authentication could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Authentication->read(null, $id);
		}
		$users = $this->Authentication->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Authentication->id = $id;
		if (!$this->Authentication->exists()) {
			throw new NotFoundException(__('Invalid authentication'));
		}
		if ($this->Authentication->delete()) {
			$this->Session->setFlash(__('Authentication deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Authentication was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Authentication->recursive = 0;
		$this->set('authentications', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Authentication->id = $id;
		if (!$this->Authentication->exists()) {
			throw new NotFoundException(__('Invalid authentication'));
		}
		$this->set('authentication', $this->Authentication->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Authentication->create();
			if ($this->Authentication->save($this->request->data)) {
				$this->Session->setFlash(__('The authentication has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The authentication could not be saved. Please, try again.'));
			}
		}
		$users = $this->Authentication->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Authentication->id = $id;
		if (!$this->Authentication->exists()) {
			throw new NotFoundException(__('Invalid authentication'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Authentication->save($this->request->data)) {
				$this->Session->setFlash(__('The authentication has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The authentication could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Authentication->read(null, $id);
		}
		$users = $this->Authentication->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Authentication->id = $id;
		if (!$this->Authentication->exists()) {
			throw new NotFoundException(__('Invalid authentication'));
		}
		if ($this->Authentication->delete()) {
			$this->Session->setFlash(__('Authentication deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Authentication was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
