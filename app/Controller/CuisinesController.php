<?php
App::uses('AppController', 'Controller');
/**
 * Cuisines Controller
 *
 * @property Cuisine $Cuisine
 */
class CuisinesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Cuisine->recursive = 0;
		$this->set('cuisines', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Cuisine->id = $id;
		if (!$this->Cuisine->exists()) {
			throw new NotFoundException(__('Invalid cuisine'));
		}
		$this->set('cuisine', $this->Cuisine->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cuisine->create();
			if ($this->Cuisine->save($this->request->data)) {
				$this->Session->setFlash(__('The cuisine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cuisine could not be saved. Please, try again.'));
			}
		}
		$restaurants = $this->Cuisine->Restaurant->find('list');
		$this->set(compact('restaurants'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Cuisine->id = $id;
		if (!$this->Cuisine->exists()) {
			throw new NotFoundException(__('Invalid cuisine'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Cuisine->save($this->request->data)) {
				$this->Session->setFlash(__('The cuisine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cuisine could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Cuisine->read(null, $id);
		}
		$restaurants = $this->Cuisine->Restaurant->find('list');
		$this->set(compact('restaurants'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Cuisine->id = $id;
		if (!$this->Cuisine->exists()) {
			throw new NotFoundException(__('Invalid cuisine'));
		}
		if ($this->Cuisine->delete()) {
			$this->Session->setFlash(__('Cuisine deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Cuisine was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Cuisine->recursive = 0;
		$this->set('cuisines', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Cuisine->id = $id;
		if (!$this->Cuisine->exists()) {
			throw new NotFoundException(__('Invalid cuisine'));
		}
		$this->set('cuisine', $this->Cuisine->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Cuisine->create();
			if ($this->Cuisine->save($this->request->data)) {
				$this->Session->setFlash(__('The cuisine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cuisine could not be saved. Please, try again.'));
			}
		}
		$restaurants = $this->Cuisine->Restaurant->find('list');
		$this->set(compact('restaurants'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Cuisine->id = $id;
		if (!$this->Cuisine->exists()) {
			throw new NotFoundException(__('Invalid cuisine'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Cuisine->save($this->request->data)) {
				$this->Session->setFlash(__('The cuisine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cuisine could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Cuisine->read(null, $id);
		}
		$restaurants = $this->Cuisine->Restaurant->find('list');
		$this->set(compact('restaurants'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Cuisine->id = $id;
		if (!$this->Cuisine->exists()) {
			throw new NotFoundException(__('Invalid cuisine'));
		}
		if ($this->Cuisine->delete()) {
			$this->Session->setFlash(__('Cuisine deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Cuisine was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
