<?php
App::uses('AppController', 'Controller');
/**
 * RestaurantsCuisines Controller
 *
 * @property RestaurantsCuisine $RestaurantsCuisine
 */
class RestaurantsCuisinesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RestaurantsCuisine->recursive = 0;
		$this->set('restaurantsCuisines', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->RestaurantsCuisine->id = $id;
		if (!$this->RestaurantsCuisine->exists()) {
			throw new NotFoundException(__('Invalid restaurants cuisine'));
		}
		$this->set('restaurantsCuisine', $this->RestaurantsCuisine->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RestaurantsCuisine->create();
			if ($this->RestaurantsCuisine->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants cuisine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants cuisine could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->RestaurantsCuisine->id = $id;
		if (!$this->RestaurantsCuisine->exists()) {
			throw new NotFoundException(__('Invalid restaurants cuisine'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RestaurantsCuisine->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants cuisine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants cuisine could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RestaurantsCuisine->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RestaurantsCuisine->id = $id;
		if (!$this->RestaurantsCuisine->exists()) {
			throw new NotFoundException(__('Invalid restaurants cuisine'));
		}
		if ($this->RestaurantsCuisine->delete()) {
			$this->Session->setFlash(__('Restaurants cuisine deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurants cuisine was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RestaurantsCuisine->recursive = 0;
		$this->set('restaurantsCuisines', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->RestaurantsCuisine->id = $id;
		if (!$this->RestaurantsCuisine->exists()) {
			throw new NotFoundException(__('Invalid restaurants cuisine'));
		}
		$this->set('restaurantsCuisine', $this->RestaurantsCuisine->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RestaurantsCuisine->create();
			if ($this->RestaurantsCuisine->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants cuisine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants cuisine could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->RestaurantsCuisine->id = $id;
		if (!$this->RestaurantsCuisine->exists()) {
			throw new NotFoundException(__('Invalid restaurants cuisine'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RestaurantsCuisine->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants cuisine has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants cuisine could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RestaurantsCuisine->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RestaurantsCuisine->id = $id;
		if (!$this->RestaurantsCuisine->exists()) {
			throw new NotFoundException(__('Invalid restaurants cuisine'));
		}
		if ($this->RestaurantsCuisine->delete()) {
			$this->Session->setFlash(__('Restaurants cuisine deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurants cuisine was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
