<?php
App::uses('AppController', 'Controller');
/**
 * Restaurants Controller
 *
 * @property Restaurant $Restaurant
 */
class RestaurantsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index($filter = null) {
		$this->Restaurant->recursive = 0;
		$params = null;

		if( $filter ){
			switch($filter){
				case 'by_neighborhood':
					$user = $this->Auth->user();
					if(empty($user['User']['neighborhood_id'])){
						$this->Session->setFlash('ATENÇÃO! Você ainda não configurou seu endereço.');
						$this->redirect('/users/mapa?return_to=/restaurants/index/by_neighborhood');
						exit;
					}else{
						$neighborhood_id = $user['User']['neighborhood_id'];
						$this->_loadByNeighborhood($neighborhood_id);
					}
					break;
				case 'by_cuisine':
					$cuisine_slug = $this->request->params['named']['cuisine'];
					$this->_loadByCuisine($cuisine_slug);
					break;
				default:
					$this->_loadAll();
			}
		}
	}

	private function _loadByNeighborhood($neighborhood_id){
		$this->loadModel('Neighborhood');						
		$this->set('neighborhood', $this->Neighborhood->read(null, $neighborhood_id));

		$restaurantsByNeighborhood = $this->Restaurant->RestaurantsNeighborhood->find('all', array(
			'conditions' => array(
				'RestaurantsNeighborhood.neighborhood_id' => $neighborhood_id
			)
		) );
		
		$ids = array();
		foreach($restaurantsByNeighborhood as $item){
			$ids[] = $item['RestaurantsNeighborhood']['restaurant_id'];
		}

		$params = array(
			'Restaurant.id' => $ids
		);

		$this->set('restaurants', $this->paginate($params));
	}

	private function _loadByCuisine($cuisine_slug){
		$this->loadModel('Cuisine');						
		$cuisine = $this->Cuisine->findBySlug($cuisine_slug);
		$this->set('cuisine', $cuisine);

		$restaurantsByCuisine = $this->Restaurant->RestaurantsCuisine->find('all', array(
			'conditions' => array(
				'RestaurantsCuisine.cuisine_id' => $cuisine['Cuisine']['id']
			)
		) );
		
		$ids = array();
		foreach($restaurantsByCuisine as $item){
			$ids[] = $item['RestaurantsCuisine']['restaurant_id'];
		}

		$params = array(
			'Restaurant.id' => $ids
		);

		$this->set('restaurants', $this->paginate($params));
	}

	private function _loadAll(){
		$this->set('restaurants', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Restaurant->recursive = 2;
		$this->Restaurant->id = $id;
		if (!$this->Restaurant->exists()) {
			throw new NotFoundException(__('Invalid restaurant'));
		}
		$restaurant = $this->Restaurant->read(null, $id);
		$this->set('restaurant', $restaurant);
		$this->set('title_for_layout', $restaurant['Restaurant']['name']);
		$this->set('ratings', $this->Restaurant->getRatings($restaurant['Restaurant']['id']));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Restaurant->create();
			if ($this->Restaurant->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant could not be saved. Please, try again.'));
			}
		}
		$users = $this->Restaurant->User->find('list');
		$cuisines = $this->Restaurant->Cuisine->find('list');
		$neighborhoods = $this->Restaurant->Neighborhood->find('list');
		$products = $this->Restaurant->Product->find('list');
		$this->set(compact('users', 'cuisines', 'neighborhoods', 'products'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Restaurant->id = $id;
		if (!$this->Restaurant->exists()) {
			throw new NotFoundException(__('Invalid restaurant'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Restaurant->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Restaurant->read(null, $id);
		}
		$users = $this->Restaurant->User->find('list');
		$cuisines = $this->Restaurant->Cuisine->find('list');
		$neighborhoods = $this->Restaurant->Neighborhood->find('list');
		$products = $this->Restaurant->Product->find('list');
		$this->set(compact('users', 'cuisines', 'neighborhoods', 'products'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Restaurant->id = $id;
		if (!$this->Restaurant->exists()) {
			throw new NotFoundException(__('Invalid restaurant'));
		}
		if ($this->Restaurant->delete()) {
			$this->Session->setFlash(__('Restaurant deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurant was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Restaurant->recursive = 0;
		$this->set('restaurants', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Restaurant->id = $id;
		if (!$this->Restaurant->exists()) {
			throw new NotFoundException(__('Invalid restaurant'));
		}
		$this->set('restaurant', $this->Restaurant->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Restaurant->create();
			if ($this->Restaurant->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant could not be saved. Please, try again.'));
			}
		}
		$users = $this->Restaurant->User->find('list');
		$cuisines = $this->Restaurant->Cuisine->find('list');
		$neighborhoods = $this->Restaurant->Neighborhood->find('list');
		$products = $this->Restaurant->Product->find('list');
		$this->set(compact('users', 'cuisines', 'neighborhoods', 'products'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Restaurant->id = $id;
		if (!$this->Restaurant->exists()) {
			throw new NotFoundException(__('Invalid restaurant'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Restaurant->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Restaurant->read(null, $id);
		}
		$users = $this->Restaurant->User->find('list');
		$cuisines = $this->Restaurant->Cuisine->find('list');
		$neighborhoods = $this->Restaurant->Neighborhood->find('list');
		$products = $this->Restaurant->Product->find('list');
		$this->set(compact('users', 'cuisines', 'neighborhoods', 'products'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Restaurant->id = $id;
		if (!$this->Restaurant->exists()) {
			throw new NotFoundException(__('Invalid restaurant'));
		}
		if ($this->Restaurant->delete()) {
			$this->Session->setFlash(__('Restaurant deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurant was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function admin_edit_info(){
		$user = $this->Auth->user();
		
		$this->Restaurant->recursive = -1;
		$restaurante = $this->Restaurant->find('first', array('conditions' => array(
			'Restaurant.user_id' => $user['id']
			)));

		if (!$restaurante) {
			throw new NotFoundException('Usuário não tem restaurante');
		}else{
			$this->redirect( array(
				'controller' => 'restaurants',
				'action' => 'edit',
				$restaurante['Restaurant']['id']
				) );
		}
	}

	public function products($restaurant_id){
		$this->view($restaurant_id);
	}
}
