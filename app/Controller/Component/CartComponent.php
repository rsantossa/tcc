<?php
App::uses('Component', 'Controller');

class CartComponent extends Component {

	function add($product, $qty){
	 	$cart = SessionComponent::read('Cart');

		if( is_array($cart) ){
			if($this->exists($product['Product']['id'])){
				$max = count($cart);
				for($i = 0; $i < $max; $i++){
					if($product['Product']['id'] == $cart[$i]['Product']['id']){
						$cart[$i]['qty'] += $qty;
						break;
					}
				}				
			}else{
				$max = count($cart);		
				$cart[$max]['Product'] = $product['Product'];
				$cart[$max]['qty'] = $qty;
			}
		}
		else{
			$cart = array();
			$cart[0]['Product'] = $product['Product'];
			$cart[0]['qty'] = $qty; 
		}
		SessionComponent::write('Cart', $cart);
	}

	function exists($product_id){
		$product_id = intval($product_id);

		$cart = SessionComponent::Read('Cart');
		$max = count($cart);
		$flag = 0;

		for($i = 0; $i < $max; $i++){
			if($product_id == $cart[$i]['Product']['id']){
				$flag = 1;
				break;
			}
		}
		return $flag;
	}

	function remove($product_id){
		$product_id = intval($product_id);
		$cart = SessionComponent::Read('Cart');
		$max = count($cart);

		for($i = 0; $i < $max; $i++){
			if($product_id == $cart[$i]['Product']['id']){
				unset($cart[$i]);
				break;
			}
		}
		SessionComponent::write('Cart', array_values($cart));
	}

}