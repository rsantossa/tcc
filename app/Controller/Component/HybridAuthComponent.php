<?php
App::uses('Component', 'Controller');

require_once( WWW_ROOT . 'hybridauth/Hybrid/Auth.php' );

class HybridAuthComponent extends Component {

	private static $config;
	private static $auth = false;

    public function __construct() {
		$this->_loadConfig();
    }

    private function _loadConfig(){
    	self::$config = array(
			"base_url" => 'http://'.$_SERVER['HTTP_HOST'].$this->base . "/hybridauth/", // well watev, set yours

			"providers" => array(

				"Facebook" => array (
					"enabled" => true,
					"keys"    => array ( "id" => "144392645614609", "secret" => "b8a35880687c4c87da7c57fe140257d4" ),
					"scope"   => "email, friends_groups, friends_hometown, friends_location, friends_photos, friends_relationship_details, friends_relationships, friends_status, friends_website, photo_upload, publish_actions, publish_stream, read_friendlists, read_insights, read_stream, share_item, status_update, user_about_me, user_activities, user_birthday, user_checkins, user_education_history, user_events, user_games_activity, user_groups, user_hometown, user_interests, user_likes, user_location, user_notes, user_online_presence, user_photo_video_tags, user_photos, user_questions, user_relationship_details, user_relationships, user_status, user_website", 
					"display" => "touch"
				),

				"Twitter" => array ( 
					"enabled" => true,
					"keys"    => array ( "key" => "h8XtX6q4f2qOydEpc4TIFw", "secret" => "uXzTCTkreNPIKSppDxSCLk2KmdzhmgmwrXf4XmK7oE" ) 
				),

				"Google" => array ( 
					"enabled" => true,
					"keys"    => array ( "id" => "648055482656", "secret" => "l7h7flUhvdePgIDZELjNsUQ6" ) 
				),
			)
		);
    }

    public static function getInstance(){
    	if( self::$auth === false ){
    		self::$auth = new Hybrid_Auth( self::$config );
    	}

    	return self::$auth;
    }

    public function error( Exception $e ){
    	echo "Ooophs, we got an error: " . $e->getMessage();
     	echo " Error code: " . $e->getCode();
     	die;
    }

    public function postOnWall($adapter, $params = array()){
    	$response = $adapter->api()->api("/me/links", "post", array(
			'message' => @$params['message'],
			'link' => @$params['link']
		));
    }

}