<?php
App::uses('AppController', 'Controller');
/**
 * ProductRatings Controller
 *
 * @property ProductRating $ProductRating
 */
class ProductRatingsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProductRating->recursive = 0;
		$this->set('productRatings', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ProductRating->id = $id;
		if (!$this->ProductRating->exists()) {
			throw new NotFoundException(__('Invalid product rating'));
		}
		$this->set('productRating', $this->ProductRating->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProductRating->create();
			if ($this->ProductRating->save($this->request->data)) {
				$this->Session->setFlash(__('The product rating has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product rating could not be saved. Please, try again.'));
			}
		}
		$users = $this->ProductRating->User->find('list');
		$products = $this->ProductRating->Product->find('list');
		$this->set(compact('users', 'products'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ProductRating->id = $id;
		if (!$this->ProductRating->exists()) {
			throw new NotFoundException(__('Invalid product rating'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ProductRating->save($this->request->data)) {
				$this->Session->setFlash(__('The product rating has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product rating could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ProductRating->read(null, $id);
		}
		$users = $this->ProductRating->User->find('list');
		$products = $this->ProductRating->Product->find('list');
		$this->set(compact('users', 'products'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ProductRating->id = $id;
		if (!$this->ProductRating->exists()) {
			throw new NotFoundException(__('Invalid product rating'));
		}
		if ($this->ProductRating->delete()) {
			$this->Session->setFlash(__('Product rating deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Product rating was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ProductRating->recursive = 0;
		$this->set('productRatings', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->ProductRating->id = $id;
		if (!$this->ProductRating->exists()) {
			throw new NotFoundException(__('Invalid product rating'));
		}
		$this->set('productRating', $this->ProductRating->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ProductRating->create();
			if ($this->ProductRating->save($this->request->data)) {
				$this->Session->setFlash(__('The product rating has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product rating could not be saved. Please, try again.'));
			}
		}
		$users = $this->ProductRating->User->find('list');
		$products = $this->ProductRating->Product->find('list');
		$this->set(compact('users', 'products'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->ProductRating->id = $id;
		if (!$this->ProductRating->exists()) {
			throw new NotFoundException(__('Invalid product rating'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ProductRating->save($this->request->data)) {
				$this->Session->setFlash(__('The product rating has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product rating could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ProductRating->read(null, $id);
		}
		$users = $this->ProductRating->User->find('list');
		$products = $this->ProductRating->Product->find('list');
		$this->set(compact('users', 'products'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ProductRating->id = $id;
		if (!$this->ProductRating->exists()) {
			throw new NotFoundException(__('Invalid product rating'));
		}
		if ($this->ProductRating->delete()) {
			$this->Session->setFlash(__('Product rating deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Product rating was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function rate($finger){
		$this->ProductRating->save( array(
			'product_id' => $this->request->params['named']['product'],
			'user_id' => $this->Auth->user('User.id'),
			$finger.'s' => 1
			) );

		$rating = $this->ProductRating->read();
		$this->_shareRating($rating);		
		$this->_saveActivity($rating);

		$this->Session->setFlash('Voto gravado e publicado na rede social: '.$this->Session->read('current_provider'));
		$this->redirect(array(
			'controller' => 'products', 
			'action' => 'view', 
			$this->request->params['named']['product']));
	}

	public function _shareRating($rating){
		$message = sprintf(
			'%s o produto "%s!"',
			$rating['ProductRating']['likes'] ? 'Curti' : 'Não curti',
			$rating['Product']['name']
			);

		$auth = $this->HybridAuth->getInstance();
		$adapter = $auth->getAdapter( $this->Session->read('current_provider') );

		$this->HybridAuth->postOnWall($adapter, array(
			'message' => $message,
			'link' => 'http://restauranteweb.com.br'
		));
	}

	public function _saveActivity($rating){		
		$this->loadModel('Activity');
		$this->Activity->save(array(
			'user_id' => $rating['ProductRating']['user_id'],
			'action' => $rating['ProductRating']['likes'] ? 'recomendou' : 'não recomenda',
			'target_type' => 'Product',
			'target_id' => $rating['ProductRating']['product_id']
		));
	}
}
