<?php
App::uses('AppController', 'Controller');
/**
 * RestaurantReviews Controller
 *
 * @property RestaurantReview $RestaurantReview
 */
class RestaurantReviewsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RestaurantReview->recursive = 0;
		$this->set('restaurantReviews', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->RestaurantReview->id = $id;
		if (!$this->RestaurantReview->exists()) {
			throw new NotFoundException(__('Invalid restaurant review'));
		}
		$this->set('restaurantReview', $this->RestaurantReview->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RestaurantReview->create();
			if ($this->RestaurantReview->save($this->request->data)) {
				if ( $this->request->is('ajax') ) {
					$review = $this->RestaurantReview->read();
					$this->_shareReview($review);
					$this->_saveActivity($review);
					$this->_printReview($review);
				}else{
					$this->Session->setFlash(__('The restaurant review has been saved'));
					$this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('The restaurant review could not be saved. Please, try again.'));
			}
		}
		$users = $this->RestaurantReview->User->find('list');
		$restaurants = $this->RestaurantReview->Restaurant->find('list');
		$this->set(compact('users', 'restaurants'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->RestaurantReview->id = $id;
		if (!$this->RestaurantReview->exists()) {
			throw new NotFoundException(__('Invalid restaurant review'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RestaurantReview->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant review has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant review could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RestaurantReview->read(null, $id);
		}
		$users = $this->RestaurantReview->User->find('list');
		$restaurants = $this->RestaurantReview->Restaurant->find('list');
		$this->set(compact('users', 'restaurants'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RestaurantReview->id = $id;
		if (!$this->RestaurantReview->exists()) {
			throw new NotFoundException(__('Invalid restaurant review'));
		}
		if ($this->RestaurantReview->delete()) {
			$this->Session->setFlash(__('Restaurant review deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurant review was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RestaurantReview->recursive = 0;
		$this->set('restaurantReviews', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->RestaurantReview->id = $id;
		if (!$this->RestaurantReview->exists()) {
			throw new NotFoundException(__('Invalid restaurant review'));
		}
		$this->set('restaurantReview', $this->RestaurantReview->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RestaurantReview->create();
			if ($this->RestaurantReview->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant review has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant review could not be saved. Please, try again.'));
			}
		}
		$users = $this->RestaurantReview->User->find('list');
		$restaurants = $this->RestaurantReview->Restaurant->find('list');
		$this->set(compact('users', 'restaurants'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->RestaurantReview->id = $id;
		if (!$this->RestaurantReview->exists()) {
			throw new NotFoundException(__('Invalid restaurant review'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RestaurantReview->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant review has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant review could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RestaurantReview->read(null, $id);
		}
		$users = $this->RestaurantReview->User->find('list');
		$restaurants = $this->RestaurantReview->Restaurant->find('list');
		$this->set(compact('users', 'restaurants'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RestaurantReview->id = $id;
		if (!$this->RestaurantReview->exists()) {
			throw new NotFoundException(__('Invalid restaurant review'));
		}
		if ($this->RestaurantReview->delete()) {
			$this->Session->setFlash(__('Restaurant review deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurant review was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function _shareReview($review){
		$message = sprintf(
			'Deixei minha opinião sobre o restaurante %s: "%s"',
			$review['Restaurant']['name'],
			$review['RestaurantReview']['body']
			);

		$auth = $this->HybridAuth->getInstance();
		$adapter = $auth->getAdapter( $this->Session->read('current_provider') );

		$this->HybridAuth->postOnWall($adapter, array(
			'message' => $message,
			'link' => 'http://restauranteweb.com.br'
		));
	}

	public function _saveActivity($review){
		$this->loadModel('Activity');
		$this->Activity->save(array(
			'user_id' => $this->request->data['RestaurantReview']['user_id'],
			'action' => 'comentou sobre',
			'target_type' => 'Restaurant',
			'target_id' => $this->request->data['RestaurantReview']['restaurant_id']
		));
	}

	public function _printReview($review){
		echo "<div class=\"comment\" style=\"display:none\">
			<strong>{$review['User']['name']}</strong> disse:
			\"{$review['RestaurantReview']['body']}\" - 
			<em>agora</em>
		</div>
		<script type=\"text/javascript\">
		$('#RestaurantReviewViewForm')[0].reset();
		$('.comment:first').slideDown();
		</script>";
		die;
	}
}
