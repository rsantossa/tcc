<?php
App::uses('AppController', 'Controller');
/**
 * RestaurantsProducts Controller
 *
 * @property RestaurantsProduct $RestaurantsProduct
 */
class RestaurantsProductsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RestaurantsProduct->recursive = 0;
		$this->set('restaurantsProducts', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->RestaurantsProduct->id = $id;
		if (!$this->RestaurantsProduct->exists()) {
			throw new NotFoundException(__('Invalid restaurants product'));
		}
		$this->set('restaurantsProduct', $this->RestaurantsProduct->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RestaurantsProduct->create();
			if ($this->RestaurantsProduct->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants product has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants product could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->RestaurantsProduct->id = $id;
		if (!$this->RestaurantsProduct->exists()) {
			throw new NotFoundException(__('Invalid restaurants product'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RestaurantsProduct->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants product has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants product could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RestaurantsProduct->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RestaurantsProduct->id = $id;
		if (!$this->RestaurantsProduct->exists()) {
			throw new NotFoundException(__('Invalid restaurants product'));
		}
		if ($this->RestaurantsProduct->delete()) {
			$this->Session->setFlash(__('Restaurants product deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurants product was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RestaurantsProduct->recursive = 0;
		$this->set('restaurantsProducts', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->RestaurantsProduct->id = $id;
		if (!$this->RestaurantsProduct->exists()) {
			throw new NotFoundException(__('Invalid restaurants product'));
		}
		$this->set('restaurantsProduct', $this->RestaurantsProduct->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RestaurantsProduct->create();
			if ($this->RestaurantsProduct->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants product has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants product could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->RestaurantsProduct->id = $id;
		if (!$this->RestaurantsProduct->exists()) {
			throw new NotFoundException(__('Invalid restaurants product'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RestaurantsProduct->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants product has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants product could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RestaurantsProduct->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RestaurantsProduct->id = $id;
		if (!$this->RestaurantsProduct->exists()) {
			throw new NotFoundException(__('Invalid restaurants product'));
		}
		if ($this->RestaurantsProduct->delete()) {
			$this->Session->setFlash(__('Restaurants product deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurants product was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
