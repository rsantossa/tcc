<?php
App::uses('AppController', 'Controller');
/**
 * RestaurantRatings Controller
 *
 * @property RestaurantRating $RestaurantRating
 */
class RestaurantRatingsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RestaurantRating->recursive = 0;
		$this->set('restaurantRatings', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->RestaurantRating->id = $id;
		if (!$this->RestaurantRating->exists()) {
			throw new NotFoundException(__('Invalid restaurant rating'));
		}
		$this->set('restaurantRating', $this->RestaurantRating->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RestaurantRating->create();
			if ($this->RestaurantRating->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant rating has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant rating could not be saved. Please, try again.'));
			}
		}
		$restaurants = $this->RestaurantRating->Restaurant->find('list');
		$users = $this->RestaurantRating->User->find('list');
		$this->set(compact('restaurants', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->RestaurantRating->id = $id;
		if (!$this->RestaurantRating->exists()) {
			throw new NotFoundException(__('Invalid restaurant rating'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RestaurantRating->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant rating has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant rating could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RestaurantRating->read(null, $id);
		}
		$restaurants = $this->RestaurantRating->Restaurant->find('list');
		$users = $this->RestaurantRating->User->find('list');
		$this->set(compact('restaurants', 'users'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RestaurantRating->id = $id;
		if (!$this->RestaurantRating->exists()) {
			throw new NotFoundException(__('Invalid restaurant rating'));
		}
		if ($this->RestaurantRating->delete()) {
			$this->Session->setFlash(__('Restaurant rating deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurant rating was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RestaurantRating->recursive = 0;
		$this->set('restaurantRatings', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->RestaurantRating->id = $id;
		if (!$this->RestaurantRating->exists()) {
			throw new NotFoundException(__('Invalid restaurant rating'));
		}
		$this->set('restaurantRating', $this->RestaurantRating->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RestaurantRating->create();
			if ($this->RestaurantRating->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant rating has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant rating could not be saved. Please, try again.'));
			}
		}
		$restaurants = $this->RestaurantRating->Restaurant->find('list');
		$users = $this->RestaurantRating->User->find('list');
		$this->set(compact('restaurants', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->RestaurantRating->id = $id;
		if (!$this->RestaurantRating->exists()) {
			throw new NotFoundException(__('Invalid restaurant rating'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RestaurantRating->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant rating has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant rating could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RestaurantRating->read(null, $id);
		}
		$restaurants = $this->RestaurantRating->Restaurant->find('list');
		$users = $this->RestaurantRating->User->find('list');
		$this->set(compact('restaurants', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RestaurantRating->id = $id;
		if (!$this->RestaurantRating->exists()) {
			throw new NotFoundException(__('Invalid restaurant rating'));
		}
		if ($this->RestaurantRating->delete()) {
			$this->Session->setFlash(__('Restaurant rating deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurant rating was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function rate($finger){
		$this->RestaurantRating->save( array(
			'restaurant_id' => $this->request->params['named']['restaurant'],
			'user_id' => $this->Auth->user('User.id'),
			$finger.'s' => 1
			) );

		$rating = $this->RestaurantRating->read();
		$this->_shareRating($rating);
		$this->_saveActivity($rating);

		$this->Session->setFlash('Voto gravado e publicado na rede social: '.$this->Session->read('current_provider'));
		$this->redirect(array(
			'controller' => 'restaurants', 
			'action' => 'view', 
			$this->request->params['named']['restaurant']));
	}

	public function _shareRating($rating){
		$message = sprintf(
			'%s o restaurante "%s!"',
			$rating['RestaurantRating']['likes'] ? 'Curti' : 'Não curti',
			$rating['Restaurant']['name']
			);

		$auth = $this->HybridAuth->getInstance();
		$adapter = $auth->getAdapter( $this->Session->read('current_provider') );

		$this->HybridAuth->postOnWall($adapter, array(
			'message' => $message,
			'link' => 'http://restauranteweb.com.br'
		));
	}

	public function _saveActivity($rating){		
		$this->loadModel('Activity');
		$this->Activity->save(array(
			'user_id' => $rating['RestaurantRating']['user_id'],
			'action' => $rating['RestaurantRating']['likes'] ? 'recomendou' : 'não recomenda',
			'target_type' => 'Restaurant',
			'target_id' => $rating['RestaurantRating']['restaurant_id']
		));
	}
}
