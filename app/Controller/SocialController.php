<?php
// session_start();

class SocialController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();	    
	    $this->Auth->allow('*');
	}

	public function index() {
		//
	} 
	
	public function login( $provider ) {
		try{
			$auth = $this->HybridAuth->getInstance();
			$adapter = $auth->authenticate( $provider );
			$this->Session->write('current_provider', $provider);
			$this->redirect('/social/login_callback');
		}
		catch( Exception $e ){
			$this->HybridAuth->error($e);
		} 
	}

	public function login_callback(){
		try{
			$auth = $this->HybridAuth->getInstance();
	        $adapter = $auth->getAdapter( $this->Session->read('current_provider') );
	        $hybridUser = $adapter->getUserProfile();

	        if( $this->Auth->loggedIn() ){
	        	$this->redirect( '/' );

	        }else{
	        	$this->loadModel('User');
	        	$user = $this->User->findByEmail( $hybridUser->email );

	        	if(!$user){
	        		$user = $this->User->register( $hybridUser );
	        	}

	        	$user['Profile'] = (array)$hybridUser;
	        	$this->Auth->login( $user );

	        	// if( !isset($user['User']['neighborhood']) ){
	         //        $redirect = '/users/mapa';
	         //    }else{
	            	$redirect = '/';
	            // }

	            $this->redirect( $redirect );
	        }

        }
		catch( Exception $e ){			
			$this->HybridAuth->error($e);
		}
	}

	public function logout() {
		try{
			$auth = $this->HybridAuth->getInstance();
			$auth->logoutAllProviders();
			$this->Auth->logout();
			$this->Session->destroy();
			$this->redirect('/');
		}
		catch( Exception $e ){			
			$this->HybridAuth->error($e);
		} 
	}

	public function share(){
		try{
			$auth = $this->HybridAuth->getInstance();
	     	$facebook = $auth->getAdapter( $this->Session->read('current_provider') );

	        // Post to the user wall 
	        $response = $facebook->api()->api("/me/feed", "post", array(
				'message' => "Hi there 2",
				//'picture' => "http://www.mywebsite.com/path/to/an/image.jpg",
				//'link' => "http://www.mywebsite.com/path/to/a/page/",
				'name' => "My page name",
				'caption' => "And caption"
			));

	        $this->layout = false;
	        $this->render(false);
	    }
   		catch( Exception $e ){
       		$this->HybridAuth->error($e);	
   		}

   		echo 'Postado';
	}

	public function publicar() {

		$page_id = '577126948';
		$access_token = 'AAACDUwhZAnBEBAOYVmEAv5knUptQdZCiBFSIk9olbNcZAh8Jj3kDP5hhfYN2QzaxZBcqpUwiJZCuRP5jGsOLSUJwLYQV27qoZD';
		$message = $this->params->query['message'];

		$params = array(
			'access_token' => $access_token, 
			'message' => $message
		);
		
		$result = "";

		$post_url = 'https://graph.facebook.com/' . $page_id . '/feed';
		
	    if ( function_exists('curl_init') ) {

			//open connection
			$ch = curl_init();

			//set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $post_url);
			curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	 
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			//execute post
			$result = curl_exec($ch);

	      	curl_close($ch);
	    } 

	    debug($result);

	    die;
	}

	public function post(){
		try{
			$auth = $this->HybridAuth->getInstance();
			$adapter = $auth->authenticate( 'Twitter' );

			$adapter->setUserStatus( "Teste de integração FF." ); 
		}
		catch( Exception $e ){			
			$this->HybridAuth->error($e);
		}
	}
}
