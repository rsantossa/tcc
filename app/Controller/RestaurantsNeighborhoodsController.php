<?php
App::uses('AppController', 'Controller');
/**
 * RestaurantsNeighborhoods Controller
 *
 * @property RestaurantsNeighborhood $RestaurantsNeighborhood
 */
class RestaurantsNeighborhoodsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RestaurantsNeighborhood->recursive = 0;
		$this->set('restaurantsNeighborhoods', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->RestaurantsNeighborhood->id = $id;
		if (!$this->RestaurantsNeighborhood->exists()) {
			throw new NotFoundException(__('Invalid restaurants neighborhood'));
		}
		$this->set('restaurantsNeighborhood', $this->RestaurantsNeighborhood->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RestaurantsNeighborhood->create();
			if ($this->RestaurantsNeighborhood->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants neighborhood has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants neighborhood could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->RestaurantsNeighborhood->id = $id;
		if (!$this->RestaurantsNeighborhood->exists()) {
			throw new NotFoundException(__('Invalid restaurants neighborhood'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RestaurantsNeighborhood->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants neighborhood has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants neighborhood could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RestaurantsNeighborhood->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RestaurantsNeighborhood->id = $id;
		if (!$this->RestaurantsNeighborhood->exists()) {
			throw new NotFoundException(__('Invalid restaurants neighborhood'));
		}
		if ($this->RestaurantsNeighborhood->delete()) {
			$this->Session->setFlash(__('Restaurants neighborhood deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurants neighborhood was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RestaurantsNeighborhood->recursive = 0;
		$this->set('restaurantsNeighborhoods', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->RestaurantsNeighborhood->id = $id;
		if (!$this->RestaurantsNeighborhood->exists()) {
			throw new NotFoundException(__('Invalid restaurants neighborhood'));
		}
		$this->set('restaurantsNeighborhood', $this->RestaurantsNeighborhood->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RestaurantsNeighborhood->create();
			if ($this->RestaurantsNeighborhood->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants neighborhood has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants neighborhood could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->RestaurantsNeighborhood->id = $id;
		if (!$this->RestaurantsNeighborhood->exists()) {
			throw new NotFoundException(__('Invalid restaurants neighborhood'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RestaurantsNeighborhood->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurants neighborhood has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurants neighborhood could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RestaurantsNeighborhood->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RestaurantsNeighborhood->id = $id;
		if (!$this->RestaurantsNeighborhood->exists()) {
			throw new NotFoundException(__('Invalid restaurants neighborhood'));
		}
		if ($this->RestaurantsNeighborhood->delete()) {
			$this->Session->setFlash(__('Restaurants neighborhood deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurants neighborhood was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
