<?php
// app/Controller/UsersController.php
class UsersController extends AppController {
    public function beforeFilter() {
	    parent::beforeFilter();
        $this->Auth->allow('*');
	}

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        }
        $roles = $this->User->Role->find('list');
        $this->set(compact('roles'));
    }

    public function admin_index() {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    /* para login da área administrativa */
	public function login() {
	    if ($this->request->is('post')) {
	        if ($this->Auth->login()) {
                //$this->redirect( $this->Auth->redirect() );
                $this->redirect( '/admin' );
	        } else {
	            $this->Session->setFlash(__('Invalid username or password, try again'));
	        }
	    }
	}

    public function admin_login(){
        $this->redirect('/login');
    }

    public function admin_view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->read(null, $id));
    }


    public function admin_edit($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }
        $roles = $this->User->Role->find('list');
        $this->set(compact('roles'));
    }

    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Session->setFlash(__('User deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function mapa($return_to = '-'){ //hifen deverá ser alterado para /
        $this->set('title_for_layout', 'Localizar Usuário');
        $this->set('current_neighborhood', $this->Auth->user('Neighborhood.name'));
        $this->set('return_to', $return_to);
    }

    public function getEnderecoByCep($cep = null){
        if($cep == null)
            return null;

        $endereco = $this->User->Neighborhood->getEnderecoByCep($cep);

        echo json_encode($endereco);
        // debug($bairro); die;
        // debug( json_decode($bairro, true) ); 
        
        $this->render(false);
        $this->layout = false;
        exit;
    }

    public function salvarEndereco(){
        $user = $this->Auth->user();
        $user['User']['neighborhood_id'] = $this->request->data['neighborhood_id'];
        $user['User']['address'] = $this->request->data['address'];
        
        $return_to = !empty($this->request->data['return_to']) ? $this->request->data['return_to'] : '/';
        $this->User->save($user['User']);
        $this->Auth->login($user);
        $this->Session->setFlash('Bairro salvo!');
        $this->redirect( $return_to );
    }

    public function getBairroByNome($nome = null){
        if($nome == null)
            return null;

        $bairro = $this->User->Neighborhood->find('first', array(
            'conditions' => array('name' => $nome),
            'recursive' => 0
            ));

        echo json_encode($bairro);
        // debug($bairro); die;
        // debug( json_decode($bairro, true) ); 
        
        $this->render(false);
        $this->layout = false;
        exit;
    }
}