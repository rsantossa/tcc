<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');
App::uses('File', 'Utility'); 
App::uses('Folder', 'Utility'); 

session_start();

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	var $components = array(
	    'Session'
	    ,'Auth' => array(
	        'authorize' => array('Controller')
	    )
	    ,'HybridAuth'
	    ,'AjaxMultiUpload.Upload'
	    ,'RequestHandler'
	    ,'Cart'
	);

	var $helpers = array(
		'Html'
		,'Session'
		,'Form'
		,'AjaxMultiUpload.Upload'
		,'Thumbnail'
		,'Time'
		,'Js'
	);
	
    public function beforeFilter() {
    	$this->Session->write('Config.language', 'por');

    	$this->Auth->allow('login', 'login_callback');
    	$this->Auth->authError = ' ';
    	$this->Auth->loginError = 'Login ou Senha inválidos.';

    	if ( isset($this->params['prefix']) ) {
			// $this->theme = 'Bootstrap';
			$this->setAdminMenu();
		}
    }

    public function isAuthorized($user) {
    	if ( $this->params['prefix'] === 'admin' && !preg_match('/admin/', $user['Role']['slug']) ) {
        	$this->Session->setFlash('Você não tem perfil com acesso: admin_portal.');
            return false;
        }

        return true;
    }
	
    public function setAdminMenu(){
    	// Define your menu
	    $menu = array(
	        'admin_restaurante' => array(
	            array(
	                'title' => 'Dados do Restaurante',
	                'url' => array('controller' => 'restaurants', 'action' => 'edit_info'),
	            ),
	            array(
	                'title' => 'Produtos',
	                'url' => array('controller' => 'products', 'action' => 'index'),
	            ),
	            array(
	                'title' => 'Pedidos',
	                'url' => array('controller' => 'orders'),
	            ),
	            array(
	                'title' => 'Sair',
	                'url' => '../../sair'
	            ),
	        ),
	        'admin' => array(	            
	            array(
	                'title' => 'Usuários',
	                'url' => array('controller' => 'users'),
	                'children' => array(
	                    array(
	                        'title' => 'Perfis de Usuário',
	                        'url' => array('controller' => 'roles'),
	                    ),
	                    array(
			                'title' => 'Atividades de Usuários',
			                'url' => array('controller' => 'activities'),
			            )
	                )
	            ),	            
	            array(
	                'title' => 'Restaurante',
	                'url' => array('controller' => 'restaurants'),
	                'children' => array(
	                    array(
	                        'title' => 'Tipos de Cozinha',
	                        'url' => array('controller' => 'cuisines'),
	                    )
	                )
	            ),
	            array(
	                'title' => 'Configurações',
	                'url' => '#',
	                'children' => array(
	                    array(
	                        'title' => 'Bairros',
	                        'url' => array('controller' => 'neighborhoods'),
	                    )
	                )
	            ),
	            array(
	                'title' => 'Sair',
	                'url' => '../../sair'
	            )
	        )
	    );

		$this->set(compact('menu'));
    }            

	function beforeRender() { 
		if ($this->name == 'CakeError') {  
			$this->layout = 'error';  
		}elseif ( isset($this->params['prefix']) ) {
			$this->layout = 'admin';
		}else{
			$this->layout = 'default'; 
			//PARA CORRIGIR O ERRO DO UNDEFINED NA TELA
		}
	}

	public function _getFriendsIds($contacts){
		$ids = array();
		foreach($contacts as $contact){
			if( !empty($contact->identifier) ){
				$ids[] = $contact->identifier;
			}
		}
		return $ids;
	}
}