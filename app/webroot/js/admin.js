var Admin = {
	initMenu: function(){
		$('#menu > li')
			.bind('mouseenter', function(){
				$('ul', this).stop().slideDown('fast');
			})
			.bind('mouseleave', function(){
				$('ul', this).hide();
			})
	},

	initFancybox: function(){
		$('.attachment').fancybox({
			openEffect : 'elastic',
			openSpeed  : 150,

			closeEffect : 'elastic',
			closeSpeed  : 150
		});
	}
}

$(document).ready(function(){
	Admin.initMenu();
	Admin.initFancybox();
});