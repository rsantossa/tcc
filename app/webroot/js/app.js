$(document).bind("mobileinit", function(){
	$.mobile.autoInitializePage = false;
	$.mobile.defaultPageTransition = "fade";
	$.mobile.loadingMessage = "Aguarde...";
});

$(document).ready(function(){
	$("body").bind("orientationchange", function(event) {
		//alert("The current orientation is: "+ event.orientation);
	});

	$("body").trigger("orientationchange");
	
	$("h1").bind("taphold", function(event) {
		$('#button-creditos').trigger('click');
  	});

  	if( $('#flashMessage').length ){
  		alert( $('#flashMessage').text() );
  	}

	$(document).ajaxStart(function(){
		$.mobile.showPageLoadingMsg();
	});

	$(document).ajaxStop(function(){
		$.mobile.hidePageLoadingMsg();
	});

});

window.addEventListener('shake', function(){
	var h1 = $('h1');

	setTimeout( function(){
		h1.addClass('animated wobble');
	}, 1000);

	setTimeout( function(){
		h1.removeClass('animated wobble');
	}, 2000);

}, false);

function tabs(){
	$(document).delegate('.ui-navbar ul li > a', 'click', function () {
	    $(this).closest('.ui-navbar').find('a').removeClass('ui-navbar-btn-active');
	    $(this).addClass('ui-navbar-btn-active');
	    $('#' + $(this).attr('data-href')).show().siblings('.tab-content').hide();
	});
}