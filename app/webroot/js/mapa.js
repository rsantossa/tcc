$(function(){
	$('#cep').mask('99999-999');

	navigator.geolocation.getCurrentPosition (function (pos)
	{
		var lat = pos.coords.latitude;
		var lng = pos.coords.longitude;

		$('#lat').text (lat);
		$('#lng').text (lng);

		showByLatLng(lat, lng); 
	});
});

function getSubprefeitura(address){
	return $.map(address, function(item){
		if( item.types[0] == 'sublocality' ) 
			return item.long_name;
	});
}

function getCep(address){
	return $.map(address, function(item){
		if( item.types[0] == 'postal_code' ) 
			return item.long_name;
	});
}

var geocoder;
var map;
var infowindow = new google.maps.InfoWindow();
var marker;

function showByLatLng(lat, lng){
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng (lat, lng);
	var options = { 
		zoom : 15, 
		center : latlng, 
		mapTypeId : google.maps.MapTypeId.ROADMAP 
	};

	var $content = $("#mapa");

	$content.show();

	map = new google.maps.Map ($content[0], options);

	geocoder.geocode({'latLng': latlng}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			if (results[0]) {
				// map.setZoom(8);
				marker = new google.maps.Marker({
					position: latlng,
					map: map
				});

				$("#address").val( results[0].formatted_address );

				infowindow.setContent( results[0].formatted_address );
				infowindow.open(map, marker);

				var cep = getCep(results[0].address_components);
				
				if( cep.length == 9 ){
					$('#cep').val( cep ).data('cep-detected', cep);
				}

				var bairro = getSubprefeitura(results[0].address_components);
				
				if( bairro.length ){
					$('#bairro').text( bairro );
					updateBairroId( bairro );
				}else{
					$('#bairro').text('-');
				}
				
			} else {
				alert('Não foi possível detectar sua localização.');
			}
		} else {
			//alert('Geocoder falhou: ' + status);
		}
    });
}

function showByCep(cep) {
	if(cep == $('#cep').data('cep-detected'))
		return;

	if(cep == '')
		return;

	geocoder.geocode({ 'address': cep }, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
			
			marker = new google.maps.Marker({
				map: map,
				position: results[0].geometry.location
			});

			$("#address").val( results[0].formatted_address );

			infowindow.setContent( results[0].formatted_address );
			infowindow.open(map, marker);

			// console.log(results);

			var bairro = getSubprefeitura(results[0].address_components);
				
			if( bairro.length ){
				$('#bairro').text( bairro );
				updateBairroId( bairro );
			}else{
				$('#bairro').text('-');
				//tenta pegar no nosso banco
				$.getJSON(
					'getEnderecoByCep/' + cep,
					function(address){
						$('#bairro').text( address.Neighborhood.name );
						updateBairroId( address );
					}
				);
				
			}
		} else {
			//alert('Geocoder falhou: ' + status);
		}
	});

}

function updateBairroId(data){
	// console.log(data);
	if(data.length > 1){
		$("#neighborhood_id").val(data.Neighborhood.id);
		$('#bairro_codigo').text('- id ' + data.Neighborhood.id);
		
	}else{
		$.getJSON(
			'getBairroByNome/' + data,
			function(bairro){
				$("#neighborhood_id").val(bairro.Neighborhood.id);
				$('#bairro_codigo').text('- id ' + bairro.Neighborhood.id);
			}
		);
	}
}