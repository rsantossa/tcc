<div class="restaurantsProducts view">
<h2><?php  echo __('Restaurants Product'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($restaurantsProduct['RestaurantsProduct']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Restaurant Id'); ?></dt>
		<dd>
			<?php echo h($restaurantsProduct['RestaurantsProduct']['restaurant_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product Id'); ?></dt>
		<dd>
			<?php echo h($restaurantsProduct['RestaurantsProduct']['product_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Restaurants Product'), array('action' => 'edit', $restaurantsProduct['RestaurantsProduct']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Restaurants Product'), array('action' => 'delete', $restaurantsProduct['RestaurantsProduct']['id']), null, __('Are you sure you want to delete # %s?', $restaurantsProduct['RestaurantsProduct']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Restaurants Products'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Restaurants Product'), array('action' => 'add')); ?> </li>
	</ul>
</div>
