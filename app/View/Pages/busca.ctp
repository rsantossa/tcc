<div data-role="content">
	
	<h3 class="mt0">Pesquisar comida</h3>
	<br>
	<ul data-role="listview" data-filter="true" data-inset="true" 
		data-filter-placeholder="Digite o nome do prato">
		
		<li>
			<a href="index.html">
				<img src="<?php echo $this->base ?>/img/sample/food1.jpg" />
				<h3>Ceasar Salad<br/>R$ 10,99</h3>
			</a>
		</li>
		<li>
			<a href="index.html">
				<img src="<?php echo $this->base ?>/img/sample/food2.jpg" />
				<h3>Barco Japonês<br/>R$ 60,99</h3>
			</a>
		</li>
		<li>
			<a href="index.html">
				<img src="<?php echo $this->base ?>/img/sample/food3.jpg" />
				<h3>Cheese Burger<br/>R$ 5,99</h3>
			</a>
		</li>

	</ul>

</div>