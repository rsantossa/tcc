<div data-role="content" style="display:none">
	<span> Latitude : </span> <span id="lat"></span> <br />
	<span> Longitude : </span> <span id="lng"></span> <br />
	<a data-role="button" id="btn">Display map</a>
</div>

<div data-role="content">
	<h3 class="mt0">Detectando sua localização...</h3>

	<div style="display:none" id="mapa"></div>

	<h3>Ou se preferir, digite o CEP</h3>

	<div class="ui-grid-a">
		<div class="ui-block-a">
			<input type="text"  data-mini="true" value="_____-___" />
		</div>
		<div class="ui-block-b">
			<button data-theme="b" data-mini="true">Salvar</button>
		</div>
	</div>	
</div>

<script src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script type="text/javascript">
navigator.geolocation.getCurrentPosition (function (pos)
{
	var lat = pos.coords.latitude;
	var lng = pos.coords.longitude;
	$("#lat").text (lat);
	$("#lng").text (lng);

	showMapa(); 
});

$("#btn").bind ("click", function (event)
{
	showMapa(); 
});

function showMapa(){
	var lat = $("#lat").text();
	var lng = $("#lng").text();
	var latlng = new google.maps.LatLng (lat, lng);
	var options = { 
		zoom : 10, 
		center : latlng, 
		mapTypeId : google.maps.MapTypeId.ROADMAP 
	};

	var $content = $("#mapa");

	$content.show();

	var map = new google.maps.Map ($content[0], options);
	// $.mobile.changePage ($("#win2"));

	new google.maps.Marker ( 
	{ 
		map : map, 
		animation : google.maps.Animation.DROP,
		position : latlng  
	}); 
}
</script>