<div class="neighborhoods form">
<?php echo $this->Form->create('Neighborhood'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Neighborhood'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('city');
		echo $this->Form->input('Restaurant');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Neighborhoods'), array('action' => 'index')); ?></li>
		
	</ul>
</div>
