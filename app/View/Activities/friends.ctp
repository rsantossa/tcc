<h2>Atividades dos meus amigos do <?php echo $this->Session->read('current_provider') ?></h2>

<div class="activity-list">
<?php if( count($friends_activities) ): 
	foreach($friends_activities as $activity):
?>
<p class="item">
	<a href="#"><img src="https://graph.facebook.com/<?php echo $activity['User']['provider_uid'] ?>/picture" 
		class="user-avatar" /> 
		<?php echo $activity['User']['name'] ?></a>
	<?php echo $activity['Activity']['action'] ?>

	<a href="/<?php echo strtolower($activity['Activity']['target_type']) ?>s/view/<?php echo $activity['Activity']['target_id'] ?>">
		<?php echo $this->App->translateActivity(
							$activity['Activity']['target_type'], 
							$activity['Activity']['target_id']) ?></a>

	<span class="date"><?php echo $this->App->nicetime($activity['Activity']['created']) ?></span>
</p>
<?php endforeach;
endif ?>
</div>