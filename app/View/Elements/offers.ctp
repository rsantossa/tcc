<h3 class="mt0">Ofertas</h3>

<?php if( count($offers) ): ?>
<div class="offers-list">
	<?php foreach($offers as $product): ?>
	<div class="item">
		<a href="/products/view/<?php echo $product['Product']['id'] ?>">
			<?php 
			echo $this->Thumbnail->render(
				$product['Product']['photo']
				, array(
			        'path' => 'Product'.DS.$product['Product']['id'],
			        'width' => '100',
			        'height' => '100',
			        'resizeOption' => 'crop',
			        'quality' => '70'
				), array());
        	?>
			<span class="name"><?php echo $product['Product']['name'] ?></span><br>
			<span class="price">R$ <?php echo $product['Product']['price'] ?></span>
		</a>
	</div>
	<?php endforeach ?>
</div>
<?php endif ?>