<div class="activity-list">
<h3>Atividade de Amigos <a href="/activities/friends" class="small">ver mais</a></h3>
<?php if( count($friends_activities) ): 
	foreach($friends_activities as $activity):
		// debug($activity);
?>
<p class="item">
	<a href="#"><img src="https://graph.facebook.com/<?php echo $activity['User']['provider_uid'] ?>/picture" 
		class="user-avatar" /> 
		<?php echo $activity['User']['name'] ?></a>
	<?php echo $activity['Activity']['action'] ?>
	<a href="/<?php echo strtolower($activity['Activity']['target_type']) ?>s/view/<?php echo $activity['Activity']['target_id'] ?>">
		<?php echo $this->App->translateActivity($activity['Activity']['target_type'], $activity['Activity']['target_id']) ?></a>
	<span class="date"><?php echo $this->App->nicetime($activity['Activity']['created']) ?></span>
</p>
<?php endforeach;
endif ?>
</div>