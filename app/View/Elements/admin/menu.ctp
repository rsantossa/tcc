<ul id="menu">
<?php 
$user = $this->Session->read('Auth.User');
foreach($menu[ $user['Role']['slug'] ] as $menu){
	echo '<li>';
	echo $this->Html->link($menu['title'], $menu['url']);
	if( array_key_exists('children', $menu) ){
		echo '<ul>';
		foreach($menu['children'] as $submenu){
			echo '<li>';
			echo $this->Html->link($submenu['title'], $submenu['url']);
			echo '</li>';
		}
		echo '</ul>';
	}
	echo '</li>';
}
?>
</ul>