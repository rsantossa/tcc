<?php 
$user = $this->Session->read('Auth.User');
echo $this->Form->create('RestaurantReview'); 
echo $this->Form->hidden('user_id', array('value' => $user['User']['id']));
echo $this->Form->hidden('restaurant_id', array('value' => $restaurant['Restaurant']['id']));
echo $this->Form->input('body', array('placeholder' => 'Deixe sua opinião...', 'label' => false));

echo $this->Js->submit('Publicar opinião', array(
		'update' => '#callbackCommentAdd',
		'url' => array('controller' => 'restaurantReviews', 'action' => 'add')
		));
echo $this->Form->end(null);
echo $this->Js->writeBuffer(); 
?>
<script type="text/javascript">
$(function(){

	$(document).ajaxStart(function(){
		$.mobile.showPageLoadingMsg();
	});

	$(document).ajaxStop(function(){
		$.mobile.hidePageLoadingMsg();
	});

});
</script>

<h4 style="margin-top:20px">Opiniões dos usuários</h4>
<div id="callbackCommentAdd"></div>
<?php
if (!empty($restaurant['RestaurantReview'])):
	foreach ($restaurant['RestaurantReview'] as $restaurantReview): ?>
	<div class="comment">
		<strong><?php echo $restaurantReview['User']['name']; ?></strong> disse:
		"<?php echo $restaurantReview['body']; ?>" - 
		<em><?php echo $this->App->nicetime($restaurantReview['created']); ?></em>
	</div>
<?php endforeach; 
endif; 
?>