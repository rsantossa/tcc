<div class="photo">
	<?php 
		echo $this->Thumbnail->render(
			$restaurant['Restaurant']['logotype']
			, array(
		        'path' => 'Restaurant'.DS.$restaurant['Restaurant']['id'],
		        'width' => '90',
		        'height' => '90',
		        'resizeOption' => 'crop',
		        'quality' => '50'
			), array());
    	?>
</div>
<h2><?php echo h($restaurant['Restaurant']['name']); ?></h2>
<dl>
	<dt><?php echo __('Street'); ?></dt>
	<dd>
		<?php echo h($restaurant['Restaurant']['street']); ?>
		&nbsp;
	</dd>
	<dt><?php echo __('Email'); ?></dt>
	<dd>
		<?php echo h($restaurant['Restaurant']['email']); ?>
		&nbsp;
	</dd>
	<dt><?php echo __('Site'); ?></dt>
	<dd>
		<?php echo h($restaurant['Restaurant']['url']); ?>
		&nbsp;
	</dd>
	<dt><?php echo __('Cuisine'); ?></dt>
	<dd>
		<?php 
		if (!empty($restaurant['Cuisine'])):
			$cuisines = array();
			foreach ($restaurant['Cuisine'] as $cuisine): 
				$cuisines[] = $cuisine['name'];
			endforeach; 
			echo implode(', ', $cuisines);
		endif; ?>
		&nbsp;
	</dd>
	<dt><?php echo __('Cobertura'); ?></dt>
	<dd>
		<?php 
		if (!empty($restaurant['Neighborhood'])):
			$neighborhoods = array();
			foreach ($restaurant['Neighborhood'] as $neighborhood): 
				$neighborhoods[] = $neighborhood['name'];
			endforeach; 
			echo implode(', ', $neighborhoods);
		endif; ?>
		&nbsp;
	</dd>
</dl>