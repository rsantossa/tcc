<?php if (!empty($restaurant['Product'])): ?>
	<ul data-role="listview" data-split-icon="plus">
	<?php
		foreach ($restaurant['Product'] as $product): ?>
		<li>
			<a href="/products/view/<?php echo $product['id'] ?>" data-ajax="false">
				<?php 
				echo $this->Thumbnail->render(
					$product['photo']
					, array(
				        'path' => 'Product'.DS.$product['id'],
				        'width' => '80',
				        'height' => '80',
				        'resizeOption' => 'crop',
				        'quality' => '50'
					), array());
	        	?>
				<h3><?php echo $product['name']; ?> - R$ <?php echo $product['price']; ?></h3>
				<p><?php echo $product['description']; ?></p>
			</a>
			<a href="/orders/add_to_cart/<?php echo $product['id'] ?>" data-ajax="false">Adicionar ao pedido</a>
		</li>
	<?php endforeach; ?>
	</ul>
<?php endif; ?>