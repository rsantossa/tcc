<div class="rating">
	<a class="finger" data-ajax="false"
		href="/restaurantRatings/rate/like/restaurant:<?php echo $restaurant['Restaurant']['id'] ?>"><img src="/img/good.png" />
		<span class="count"><?php echo $ratings['likes'] ?></span>
	</a>
	<a class="finger" data-ajax="false"
		href="/restaurantRatings/rate/dislike/restaurant:<?php echo $restaurant['Restaurant']['id'] ?>"><img src="/img/bad.png" />
		<span class="count"><?php echo $ratings['dislikes'] ?></span>
	</a>
</div>