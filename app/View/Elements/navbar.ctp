<?php if( $this->Session->check('Auth.User') ): ?>
<div data-role="navbar" data-iconpos="left">
	<ul>
		<li><a href="<?php $this->base ?>/restaurants/index/by_neighborhood" data-ajax="false" data-icon="home">Na região</a></li>
		<li><a href="<?php $this->base ?>/restaurants/index/by_search" data-ajax="false" data-icon="search">Pesquisar</a></li>
		<li><a href="<?php $this->base ?>/cuisines" data-ajax="false" data-icon="grid">Cozinha</a></li>
	</ul>
</div><!-- /navbar -->
<?php endif ?>