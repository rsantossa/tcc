<div data-role="header">
	<h1><a href="<?php echo $this->base ?>/" style="color:#fff">FaceFood</a></h1>

	<?php if( $user = $this->Session->read('Auth.User') ):	?>

	<div style="display:none">
		<a href="<?php echo $this->base ?>/creditos" data-rel="dialog" data-transition="slidedown" id="button-creditos">Créditos</a>
	</div>

	<div id="user-info">
		<img class="user-avatar" src="<?php echo $user['Profile']['photoURL'] ?>" />
		<span class="user-displayName">
			<?php $parts = explode(' ', $user['Profile']['displayName']); 
			echo $parts[0] ?></span>
	</div>

	<a href="#menu-opcoes" class="ui-btn-right" 
		data-rel="popup" data-role="button" data-inline="true" data-theme="e" data-icon="star">Perfil</a>

	<div data-role="popup" id="menu-opcoes" data-theme="a">
		<ul data-role="listview" data-inset="true" data-theme="e">
			<li><a href="<?php echo $this->base ?>/users/mapa" data-ajax="false">Mudar de Bairro</a></li>
			<li><a href="#" data-ajax="false">Meus Pedidos <em style="font-size:13px">indisponível</em></a></li>
			<li><a href="#" data-ajax="false">Meu Cadastro <em style="font-size:13px">indisponível</em></a></li>
			<li><a href="<?php echo $this->base ?>/sair" data-ajax="false">Sair</a></li>
		</ul>
	</div>

	<div style="position:absolute; right:90px; top:5px"><?php echo $this->Html->image($this->Session->read('current_provider').'.png') ?></div>

	<?php endif; ?>

</div><!-- /header -->