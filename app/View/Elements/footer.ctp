<?php if( $this->Session->check('Auth.User') ): 
$cart = $this->Session->read('Cart');
$total = 0;

if( $cart ){
	foreach($cart as $item){
		$total += $item['Product']['price'] * $item['qty'];
	}
}
?>
<div data-role="footer" data-theme="d">
	<h4>Total: <?php echo 'R$ '.number_format($total, 2) ?></h4>

	<?php if( $total ): ?>
	<a href="/orders/cart" class="ui-btn-right" 
		data-role="button" data-inline="true" data-theme="e" data-icon="arrow-r">Carrinho</a>
	<?php endif ?>
</div><!-- /footer -->
<?php endif ?>