<div class="photo">
	<?php 
		echo $this->Thumbnail->render(
			$product['Product']['photo']
			, array(
		        'path' => 'Product'.DS.$product['Product']['id'],
		        'width' => '120',
		        'height' => '120',
		        'resizeOption' => 'crop',
		        'quality' => '50'
			), array());
    	?>
</div>
<a href="/restaurants/view/<?php echo $restaurant['Restaurant']['id'] ?>" 
	data-ajax="false"
	style="font-weight:normal">
	<?php echo $restaurant['Restaurant']['name'] ?></a><br>
<h2 class="name"><?php echo h($product['Product']['name']); ?></h2>
<h3 class="description"><?php echo h($product['Product']['description']); ?></h3>
<p class="price">R$ <?php echo h($product['Product']['price']); ?>
	<a href="/orders/add_to_cart/<?php echo $product['Product']['id'] ?>" data-ajax="false" 
	data-role="button" data-icon="plus" data-inline="true" data-mini="true" data-theme="b">Comprar</a>
</p>