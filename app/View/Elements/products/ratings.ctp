<div class="rating">
	<a class="finger" data-ajax="false"
		href="/productRatings/rate/like/product:<?php echo $product['Product']['id'] ?>"><img src="/img/good.png" />
		<span class="count"><?php echo $ratings['likes'] ?></span>
	</a>
	<a class="finger" data-ajax="false"
		href="/productRatings/rate/dislike/product:<?php echo $product['Product']['id'] ?>"><img src="/img/bad.png" />
		<span class="count"><?php echo $ratings['dislikes'] ?></span>
	</a>
</div>