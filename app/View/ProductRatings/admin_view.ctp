<div class="productRatings view">
<h2><?php  echo __('Product Rating'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($productRating['ProductRating']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productRating['User']['name'], array('controller' => 'users', 'action' => 'view', $productRating['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productRating['Product']['name'], array('controller' => 'products', 'action' => 'view', $productRating['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Likes'); ?></dt>
		<dd>
			<?php echo h($productRating['ProductRating']['likes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dislikes'); ?></dt>
		<dd>
			<?php echo h($productRating['ProductRating']['dislikes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($productRating['ProductRating']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Product Rating'), array('action' => 'edit', $productRating['ProductRating']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Product Rating'), array('action' => 'delete', $productRating['ProductRating']['id']), null, __('Are you sure you want to delete # %s?', $productRating['ProductRating']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Ratings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Rating'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
	</ul>
</div>
