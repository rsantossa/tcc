<div data-role="content">
	<h3 class="mt0">Escolha o Tipo de Comida</h3>
	<br>
	<ul data-role="listview">
		<?php foreach ($cuisines as $cuisine): ?>
		<li><a href="/restaurants/index/by_cuisine/cuisine:<?php echo $cuisine['Cuisine']['slug'] ?>">
			<?php echo h($cuisine['Cuisine']['name']) ?></a></li>
		<?php endforeach; ?>
	</ul>
</div>