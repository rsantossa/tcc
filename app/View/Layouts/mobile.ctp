<!DOCTYPE html> 
<html> 
	<head> 
		<title><?php echo $title_for_layout ?></title> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<?php
		echo $this->fetch('meta');

		$this->Html->css( array('themes/default/jquery.mobile-1.2.0-rc.2', 'animate', 'facefood', 'jquery.mobile.autocomplete'), null, array('inline' => false));

		echo $this->fetch('css');
		
		$this->Html->script( array('jquery', 'jquery.mobile-1.2.0-rc.2', 'app', 'jquery.mobile.alert'), array('inline' => false));

		echo $this->fetch('script');
		?>
	</head>
	<body>
		<div data-role="page">
			<?php echo $this->element('header') ?>

			<?php echo $this->element('navbar') ?>

			<div data-role="content">	
				<?php echo $this->Session->flash(); //debug($this->Session->read('Auth.User')) ?>
					
				<?php echo $this->fetch('content'); ?>
			</div><!-- /content -->

			<?php echo $this->element('footer') ?>

			<?php echo $this->element('sql_dump'); ?>
		</div><!-- /page -->

		
	</body>
</html>