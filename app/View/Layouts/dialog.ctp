<!DOCTYPE html> 
<html> 
	<head> 
		<title><?php echo $title_for_layout ?></title> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<?php
			echo $this->fetch('meta');

			$this->Html->css('themes/default/jquery.mobile-1.2.0-rc.2', null, array('inline' => false));
			
			echo $this->fetch('css');
			
			$this->Html->script('jquery', array('inline' => false));
			$this->Html->script('jquery.mobile-1.2.0-rc.2', array('inline' => false));
		?>
	</head>
	<body>
		<div data-role="dialog">

			<div data-role="header" data-theme="d">
				<h1><?php echo $title_for_layout ?></h1>
			</div>

			<div data-role="content" data-theme="c">
				<?php echo $this->fetch('content'); ?>
			</div><!-- /content -->

		</div><!-- /page -->

		<?php
		echo $this->fetch('script');
		?>
	</body>
</html>