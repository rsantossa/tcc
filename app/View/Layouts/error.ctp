<!DOCTYPE html> 
<html> 
	<head> 
		<title><?php echo $title_for_layout ?></title> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<?php
		echo $this->fetch('meta');

		$this->Html->css( array('themes/default/jquery.mobile-1.2.0-rc.2', 'animate', 'facefood', 'jquery.mobile.alert'), null, array('inline' => false));

		echo $this->fetch('css');
		echo $this->fetch('script');
		?>
	</head>
	<body>
		<div data-role="page">
			<div data-role="content">	
				<?php echo $this->Session->flash(); //debug($this->Session->read('Auth.User')) ?>
				<?php echo $this->fetch('content'); ?>
			</div><!-- /content -->
			<?php echo $this->element('sql_dump'); ?>
		</div><!-- /page -->

		
	</body>
</html>