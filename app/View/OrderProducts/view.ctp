<div class="orderProducts view">
<h2><?php  echo __('Order Product'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($orderProduct['OrderProduct']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product Id'); ?></dt>
		<dd>
			<?php echo h($orderProduct['OrderProduct']['product_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order Id'); ?></dt>
		<dd>
			<?php echo h($orderProduct['OrderProduct']['order_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($orderProduct['OrderProduct']['quantity']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Order Product'), array('action' => 'edit', $orderProduct['OrderProduct']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Order Product'), array('action' => 'delete', $orderProduct['OrderProduct']['id']), null, __('Are you sure you want to delete # %s?', $orderProduct['OrderProduct']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Order Products'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Order Product'), array('action' => 'add')); ?> </li>
	</ul>
</div>
