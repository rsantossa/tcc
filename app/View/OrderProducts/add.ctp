<div class="orderProducts form">
<?php echo $this->Form->create('OrderProduct'); ?>
	<fieldset>
		<legend><?php echo __('Add Order Product'); ?></legend>
	<?php
		echo $this->Form->input('product_id');
		echo $this->Form->input('order_id');
		echo $this->Form->input('quantity');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Order Products'), array('action' => 'index')); ?></li>
	</ul>
</div>
