<h2>Carrinho de Compras</h2>
<div style="padding:20px">
<?php 
if( $this->Session->check('Auth.User') ): 
	$cart = $this->Session->read('Cart');
	
	if( !$cart ){
		echo 'Vazio';
	}else{ ?>
		<ul data-role="listview">
			<?php
			$total = 0;
			foreach ($cart as $product): ?>
			<li>
				<a href="/products/view/<?php echo $product['Product']['id'] ?>" data-ajax="false">
					<?php 
					echo $this->Thumbnail->render(
						$product['Product']['photo']
						, array(
					        'path' => 'Product'.DS.$product['Product']['id'],
					        'width' => '80',
					        'height' => '80',
					        'resizeOption' => 'crop',
					        'quality' => '50'
						), array());
		        	?>
					<h3><?php echo $product['Product']['name']; ?> - R$ <?php echo $product['Product']['price']; ?></h3>
					<p><?php echo $product['Product']['description']; ?></p>
				</a>
				<span class="ui-li-count"><?php echo $product['qty'] ?></span>
			</li>
			<?php endforeach; ?>
		</ul>
		<br><Br>
		<a href="/orders/empty_cart" data-ajax="false" data-theme="a" 
			data-role="button" data-mini="true" data-inline="true">Esvaziar Carrinho</a>

		<a href="/orders/finish" data-ajax="false" data-theme="e" 
			data-role="button" data-mini="true" data-inline="true">Concluir Pedido</a>
	<?php
	}

endif ?>
</div>