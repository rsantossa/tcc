
<?php $user = $this->Session->read('Auth.User') ?>
<?php echo $this->Form->create('Order', array('action' => 'add_details', 'data-ajax' => 'false'));  ?>

	<h2 style="margin-bottom:0px">Endereço de Entrega</h2>
	Corrija-o se necessário<br>
	<textarea name="data[comment]"><?php echo $user['User']['address'] ?></textarea>

	<h2 style="margin-bottom:0px">Forma de Pagamento</h2>
	somente com cartão de crédito
	<fieldset data-role="controlgroup" data-mini="true">
    	<input type="radio" name="payment" id="radio-mini-1" value="VISA" checked="checked" />
    	<label for="radio-mini-1">VISA</label>

		<input type="radio" name="payment" id="radio-mini-2" value="Mastercard"  />
    	<label for="radio-mini-2">Mastercard</label>
    	
    	<input type="radio" name="payment" id="radio-mini-3" value="Amex"  />
    	<label for="radio-mini-3">Amex</label>
	</fieldset>
	<br>
	* O motoboy providenciará a "maquininha"

<?php echo $this->Form->end(__('Concluir')); ?>