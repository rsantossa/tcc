<div class="users form">
<?php echo $this->Session->flash('auth'); ?>

<div data-role="collapsible-set" data-content-theme="c">
	<div data-role="collapsible" data-theme="a" data-content-theme="b" data-collapsed="false" 
		 data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d">
		<h3>Acessar o Site</h3>

		<p>Para aproveitar as ofertas do site, faça login com sua conta nas redes sociais.</p>

		<a href="<?php echo $this->base ?>/social/login/facebook" data-ajax="false" data-role="button" data-theme="b">Login com Facebook</a>

		<a href="<?php echo $this->base ?>/social/login/twitter" data-ajax="false" data-role="button" data-theme="b">Login com Twitter</a>

		<!-- <a href="<?php echo $this->base ?>/social/login/google" data-ajax="false" data-role="button" data-theme="b">Login com Google+</a> -->
	</div>

	<div data-role="collapsible" data-theme="a" data-content-theme="b"
		 data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d">
		<h3>Acessar o Admin</h3>
		
		<?php 
		if( $this->Session->read('Auth.User') ){
			?>
			<a href="/admin" data-ajax="false">Ir para o Admin</a>
			<?php
		}else{
			echo $this->Form->create('User', array('data-ajax="false"')); 
	        echo $this->Form->input('username');
	        echo $this->Form->input('password');
			echo $this->Form->end(__('Login')); 
		}
		?>
	</div>

</div>