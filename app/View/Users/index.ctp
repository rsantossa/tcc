<h1>Blog Users</h1>
<p><?php echo $this->Html->link('Add User', array('action' => 'add')); ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Role</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>

<!-- Here's where we loop through our $users array, printing out User info -->

    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['id']; ?></td>
        <td>
            <?php echo $this->Html->link($user['User']['username'], array('action' => 'view', $user['User']['id'])); ?>
        </td>
        <td>
            <?php echo $user['User']['username']; ?>
        </td>
        <td>
            <?php echo $this->Form->postLink(
                'Delete',
                array('action' => 'delete', $user['User']['id']),
                array('confirm' => 'Are you sure?'));
            ?>
            <?php echo $this->Html->link('Edit', array('action' => 'edit', $user['User']['id'])); ?>
        </td>
        <td>
            <?php echo $user['User']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>

<table cellpadding="0" cellspacing="0">
<?php echo $this->element('table_header', array('keys'=>array('id', 'name', 'role','created', 'modified')), array('plugin'=>'ActiveAdmin')); ?>
  <?php
  $i = 0;
  foreach ($posts as $post):
    $class = null;
    if ($i++ % 2 == 0) {
      $class = ' class="even"';
    }
  ?>
  <tr<?php echo $class;?>>
    <td><?php echo $post['Post']['id']; ?>&nbsp;</td>
    <td><?php echo $post['Post']['title']; ?>&nbsp;</td>
    <td><?php echo $post['Post']['slug']; ?>&nbsp;</td>
    <td><?php echo $post['Post']['created']; ?>&nbsp;</td>
    <td><?php echo $post['Post']['modified']; ?>&nbsp;</td>
    <td class="actions">
      <?php echo $this->Html->link(__('View'), array('action' => 'view', $post['Post']['id'])); ?>
      <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $post['Post']['id'])); ?>
      <?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $post['Post']['id']), null, __('Are you sure you want to delete # %s?', $post['Post']['id'])); ?>
    </td>
  </tr>
<?php endforeach; ?>
</table>