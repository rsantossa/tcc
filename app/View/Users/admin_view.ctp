<div class="users view">
<h2><?php  echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd>
			<?php echo $this->Html->link($user['Role']['name'], array('controller' => 'roles', 'action' => 'view', $user['Role']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles'), array('controller' => 'roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Activities'), array('controller' => 'activities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Activity'), array('controller' => 'activities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Authentications'), array('controller' => 'authentications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Authentication'), array('controller' => 'authentications', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Orders'), array('controller' => 'orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Order'), array('controller' => 'orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Ratings'), array('controller' => 'product_ratings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Rating'), array('controller' => 'product_ratings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Restaurant Ratings'), array('controller' => 'restaurant_ratings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Restaurant Rating'), array('controller' => 'restaurant_ratings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Restaurant Reviews'), array('controller' => 'restaurant_reviews', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Restaurant Review'), array('controller' => 'restaurant_reviews', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Restaurants'), array('controller' => 'restaurants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Restaurant'), array('controller' => 'restaurants', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Activities'); ?></h3>
	<?php if (!empty($user['Activity'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Action'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Target Type'); ?></th>
		<th><?php echo __('Target Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Activity'] as $activity): ?>
		<tr>
			<td><?php echo $activity['id']; ?></td>
			<td><?php echo $activity['user_id']; ?></td>
			<td><?php echo $activity['action']; ?></td>
			<td><?php echo $activity['created']; ?></td>
			<td><?php echo $activity['target_type']; ?></td>
			<td><?php echo $activity['target_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'activities', 'action' => 'view', $activity['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'activities', 'action' => 'edit', $activity['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'activities', 'action' => 'delete', $activity['id']), null, __('Are you sure you want to delete # %s?', $activity['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Activity'), array('controller' => 'activities', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Authentications'); ?></h3>
	<?php if (!empty($user['Authentication'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Provider'); ?></th>
		<th><?php echo __('Provider Uid'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Profile Url'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Authentication'] as $authentication): ?>
		<tr>
			<td><?php echo $authentication['id']; ?></td>
			<td><?php echo $authentication['user_id']; ?></td>
			<td><?php echo $authentication['provider']; ?></td>
			<td><?php echo $authentication['provider_uid']; ?></td>
			<td><?php echo $authentication['email']; ?></td>
			<td><?php echo $authentication['name']; ?></td>
			<td><?php echo $authentication['profile_url']; ?></td>
			<td><?php echo $authentication['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'authentications', 'action' => 'view', $authentication['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'authentications', 'action' => 'edit', $authentication['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'authentications', 'action' => 'delete', $authentication['id']), null, __('Are you sure you want to delete # %s?', $authentication['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Authentication'), array('controller' => 'authentications', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Orders'); ?></h3>
	<?php if (!empty($user['Order'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Total'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Restaurant Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Comment'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Order'] as $order): ?>
		<tr>
			<td><?php echo $order['id']; ?></td>
			<td><?php echo $order['total']; ?></td>
			<td><?php echo $order['user_id']; ?></td>
			<td><?php echo $order['restaurant_id']; ?></td>
			<td><?php echo $order['created']; ?></td>
			<td><?php echo $order['status']; ?></td>
			<td><?php echo $order['comment']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'orders', 'action' => 'view', $order['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'orders', 'action' => 'edit', $order['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'orders', 'action' => 'delete', $order['id']), null, __('Are you sure you want to delete # %s?', $order['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Order'), array('controller' => 'orders', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Product Ratings'); ?></h3>
	<?php if (!empty($user['ProductRating'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th><?php echo __('Likes'); ?></th>
		<th><?php echo __('Dislikes'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['ProductRating'] as $productRating): ?>
		<tr>
			<td><?php echo $productRating['id']; ?></td>
			<td><?php echo $productRating['user_id']; ?></td>
			<td><?php echo $productRating['product_id']; ?></td>
			<td><?php echo $productRating['likes']; ?></td>
			<td><?php echo $productRating['dislikes']; ?></td>
			<td><?php echo $productRating['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'product_ratings', 'action' => 'view', $productRating['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'product_ratings', 'action' => 'edit', $productRating['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'product_ratings', 'action' => 'delete', $productRating['id']), null, __('Are you sure you want to delete # %s?', $productRating['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Product Rating'), array('controller' => 'product_ratings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Restaurant Ratings'); ?></h3>
	<?php if (!empty($user['RestaurantRating'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Restaurant Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Likes'); ?></th>
		<th><?php echo __('Dislikes'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['RestaurantRating'] as $restaurantRating): ?>
		<tr>
			<td><?php echo $restaurantRating['id']; ?></td>
			<td><?php echo $restaurantRating['restaurant_id']; ?></td>
			<td><?php echo $restaurantRating['user_id']; ?></td>
			<td><?php echo $restaurantRating['likes']; ?></td>
			<td><?php echo $restaurantRating['dislikes']; ?></td>
			<td><?php echo $restaurantRating['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'restaurant_ratings', 'action' => 'view', $restaurantRating['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'restaurant_ratings', 'action' => 'edit', $restaurantRating['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'restaurant_ratings', 'action' => 'delete', $restaurantRating['id']), null, __('Are you sure you want to delete # %s?', $restaurantRating['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Restaurant Rating'), array('controller' => 'restaurant_ratings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Restaurant Reviews'); ?></h3>
	<?php if (!empty($user['RestaurantReview'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Restaurant Id'); ?></th>
		<th><?php echo __('Body'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['RestaurantReview'] as $restaurantReview): ?>
		<tr>
			<td><?php echo $restaurantReview['id']; ?></td>
			<td><?php echo $restaurantReview['user_id']; ?></td>
			<td><?php echo $restaurantReview['restaurant_id']; ?></td>
			<td><?php echo $restaurantReview['body']; ?></td>
			<td><?php echo $restaurantReview['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'restaurant_reviews', 'action' => 'view', $restaurantReview['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'restaurant_reviews', 'action' => 'edit', $restaurantReview['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'restaurant_reviews', 'action' => 'delete', $restaurantReview['id']), null, __('Are you sure you want to delete # %s?', $restaurantReview['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Restaurant Review'), array('controller' => 'restaurant_reviews', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Restaurants'); ?></h3>
	<?php if (!empty($user['Restaurant'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Street'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Url'); ?></th>
		<th><?php echo __('Active'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Restaurant'] as $restaurant): ?>
		<tr>
			<td><?php echo $restaurant['id']; ?></td>
			<td><?php echo $restaurant['name']; ?></td>
			<td><?php echo $restaurant['street']; ?></td>
			<td><?php echo $restaurant['email']; ?></td>
			<td><?php echo $restaurant['url']; ?></td>
			<td><?php echo $restaurant['active']; ?></td>
			<td><?php echo $restaurant['created']; ?></td>
			<td><?php echo $restaurant['modified']; ?></td>
			<td><?php echo $restaurant['user_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'restaurants', 'action' => 'view', $restaurant['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'restaurants', 'action' => 'edit', $restaurant['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'restaurants', 'action' => 'delete', $restaurant['id']), null, __('Are you sure you want to delete # %s?', $restaurant['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Restaurant'), array('controller' => 'restaurants', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
