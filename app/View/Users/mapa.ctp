<div data-role="content">
	<h3 class="mt0" id="detectando">Detectando sua localização...</h3>

	<div style="display:none" id="mapa"></div>

	<br>
	<h3>Se preferir, digite o CEP</h3>

	<form action="/users/salvarEndereco/" method="post" data-ajax="false">
		<input type="hidden" value="<?php echo $_GET['return_to'] ?>" id="return_to" name="return_to" />

		<div class="ui-grid-a">
			<input type="text" data-mini="true" placeholder="_____-___" id="cep" onblur="showByCep(this.value)" />
		</div>

		<div id="detalhes" style="float:left; padding-top:20px">
			<strong>Latitude:</strong> <span id="lat">-</span>
			<strong> - Longitude:</strong> <span id="lng">-</span>
			<strong> - Bairro/Subprefeitura:</strong> <span id="bairro">-</span>
			<span id="bairro_codigo"></span>
			<input type="hidden" value="" id="neighborhood_id" name="neighborhood_id" />
			<input type="hidden" value="" id="address" name="address" />
		</div>

		<br style="clear:both"/>
		<?php if(!empty($current_neighborhood)): ?>
		Localização salva anteriormente: <?php echo $current_neighborhood ?><br>
		<?php endif ?>
		<button data-theme="b" data-mini="true" data-inline="true">Salvar Minha Localização</button>

	</form>
</div>

<!-- <script src="http://maps.googleapis.com/maps/api/js?sensor=true"></script> -->
<script type="text/javascript" 
		src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&language=pt"></script>
<script type="text/javascript" 
		src="<?php $this->base ?>/js/mapa.js"></script>