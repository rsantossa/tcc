<h1>Cardápio de <?php echo $restaurant['Restaurant']['name'] ?></h1>

<?php 
if (!empty($restaurant['Product'])):
	$i = 0;
	foreach ($restaurant['Product'] as $product): ?>
	<ul>
		<li><?php echo $product['id']; ?></li>
		<li><?php echo $product['name']; ?></li>
		<li><?php echo $product['description']; ?></li>
		<li><?php echo $product['price']; ?></li>
		<li><?php echo $product['created']; ?></li>
		<li><?php echo $product['modified']; ?></li>
		<li><?php echo $product['active']; ?></li>
		<li><?php echo $product['photo']; ?></li>
	</ul>
<?php endforeach; 
endif; ?>