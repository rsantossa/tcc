<div class="restaurants view">
	<?php echo $this->element('restaurants/info') ?>
	<?php echo $this->element('restaurants/ratings') ?>
	<br><br><?php echo $this->element('share') ?>
</div>

<script type="text/javascript">$(tabs);</script>

<div data-role="navbar" class="no-shadow">
	<ul>
		<li><a href="javascript:;" data-href="opinioes" class="ui-navbar-btn-active">Opiniões</a></li>
        <li><a href="javascript:;" data-href="cardapio">Cardápio</a></li>
	</ul>
</div>

<div id="opinioes" class="tab-content" style="display:none">
	<?php echo $this->element('restaurants/reviews') ?>
</div>

<div id="cardapio" class="tab-content" style="display:none">
	<?php echo $this->element('restaurants/products') ?>
</div>
