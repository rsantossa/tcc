<div class="restaurants form">
<?php echo $this->Form->create('Restaurant'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Restaurant'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('street');
		echo $this->Form->input('email');
		echo $this->Form->input('url');
		echo $this->Upload->edit('Restaurant', $this->Form->fields['Restaurant.id']);
		echo $this->Form->hidden('logotype', array('class' => 'attachment'));
		echo $this->Form->input('active');
		echo $this->Form->input('user_id');
		echo $this->Form->input('Cuisine');
		echo $this->Form->input('Neighborhood');
		// echo $this->Form->input('Product');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Restaurant.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Restaurant.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Restaurants'), array('action' => 'index')); ?></li>
		
	</ul>
</div>
