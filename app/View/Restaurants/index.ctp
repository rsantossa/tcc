<div data-role="content">	
	<?php 
	switch($this->request->params['pass'][0]):
		case 'by_neighborhood': ?>
			<h3>Restaurante que atendem no bairro:
			<?php echo $neighborhood['Neighborhood']['name'] ?></h3>
			<ul data-role="listview" data-inset="true">
		<?php break;
		case 'by_cuisine': ?>
			<h3>Restaurante do tipo de cozinha:<br>
			<?php echo $cuisine['Cuisine']['name'] ?></h3>
			<ul data-role="listview" data-inset="true">
		<?php break;
		default: ?>
			<h3>Lista de Restaurantes</h3><br>
			<ul data-role="listview" data-inset="true" data-filter="true"
				data-filter-placeholder="Pesquise o restaurante">
		<?php
	endswitch;
	?>
		<?php foreach($restaurants as $restaurant): ?>
		<li>
			<a href="/restaurants/view/<?php echo $restaurant['Restaurant']['id'] ?>">
				<h3><?php echo $restaurant['Restaurant']['name'] ?></h3>
			</a>
		</li>
		<?php endforeach ?>
	</ul>

</div>