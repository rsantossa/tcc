<div class="restaurants form">
<?php echo $this->Form->create('Restaurant'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Restaurant'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('street');
		echo $this->Form->input('email');
		echo $this->Form->input('url');
		echo $this->Upload->edit('Restaurant', 'temp');
		echo $this->Form->hidden('logotype', array('class' => 'attachment'));
		echo $this->Form->input('active');
		echo $this->Form->input('user_id', array('label' => 'Dono deste restaurante'));
		echo $this->Form->input('Cuisine');
		echo $this->Form->input('Neighborhood');
		// echo $this->Form->input('Product');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Restaurants'), array('action' => 'index')); ?></li>
		
	</ul>
</div>
