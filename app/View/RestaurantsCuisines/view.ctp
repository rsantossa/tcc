<div class="restaurantsCuisines view">
<h2><?php  echo __('Restaurants Cuisine'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($restaurantsCuisine['RestaurantsCuisine']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Restaurant Id'); ?></dt>
		<dd>
			<?php echo h($restaurantsCuisine['RestaurantsCuisine']['restaurant_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cuisine Id'); ?></dt>
		<dd>
			<?php echo h($restaurantsCuisine['RestaurantsCuisine']['cuisine_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Restaurants Cuisine'), array('action' => 'edit', $restaurantsCuisine['RestaurantsCuisine']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Restaurants Cuisine'), array('action' => 'delete', $restaurantsCuisine['RestaurantsCuisine']['id']), null, __('Are you sure you want to delete # %s?', $restaurantsCuisine['RestaurantsCuisine']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Restaurants Cuisines'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Restaurants Cuisine'), array('action' => 'add')); ?> </li>
	</ul>
</div>
