<div class="restaurantsCuisines index">
	<h2><?php echo __('Restaurants Cuisines'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('restaurant_id'); ?></th>
			<th><?php echo $this->Paginator->sort('cuisine_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($restaurantsCuisines as $restaurantsCuisine): ?>
	<tr>
		<td><?php echo h($restaurantsCuisine['RestaurantsCuisine']['id']); ?>&nbsp;</td>
		<td><?php echo h($restaurantsCuisine['RestaurantsCuisine']['restaurant_id']); ?>&nbsp;</td>
		<td><?php echo h($restaurantsCuisine['RestaurantsCuisine']['cuisine_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $restaurantsCuisine['RestaurantsCuisine']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $restaurantsCuisine['RestaurantsCuisine']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $restaurantsCuisine['RestaurantsCuisine']['id']), null, __('Are you sure you want to delete # %s?', $restaurantsCuisine['RestaurantsCuisine']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Restaurants Cuisine'), array('action' => 'add')); ?></li>
	</ul>
</div>
