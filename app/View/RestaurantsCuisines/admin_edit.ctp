<div class="restaurantsCuisines form">
<?php echo $this->Form->create('RestaurantsCuisine'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Restaurants Cuisine'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('restaurant_id');
		echo $this->Form->input('cuisine_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RestaurantsCuisine.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('RestaurantsCuisine.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Restaurants Cuisines'), array('action' => 'index')); ?></li>
	</ul>
</div>
