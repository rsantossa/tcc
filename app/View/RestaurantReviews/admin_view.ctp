<div class="restaurantReviews view">
<h2><?php  echo __('Restaurant Review'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($restaurantReview['RestaurantReview']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($restaurantReview['User']['name'], array('controller' => 'users', 'action' => 'view', $restaurantReview['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Restaurant'); ?></dt>
		<dd>
			<?php echo $this->Html->link($restaurantReview['Restaurant']['name'], array('controller' => 'restaurants', 'action' => 'view', $restaurantReview['Restaurant']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd>
			<?php echo h($restaurantReview['RestaurantReview']['body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($restaurantReview['RestaurantReview']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Restaurant Review'), array('action' => 'edit', $restaurantReview['RestaurantReview']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Restaurant Review'), array('action' => 'delete', $restaurantReview['RestaurantReview']['id']), null, __('Are you sure you want to delete # %s?', $restaurantReview['RestaurantReview']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Restaurant Reviews'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Restaurant Review'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Restaurants'), array('controller' => 'restaurants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Restaurant'), array('controller' => 'restaurants', 'action' => 'add')); ?> </li>
	</ul>
</div>
