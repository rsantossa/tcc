<div class="restaurantsNeighborhoods view">
<h2><?php  echo __('Restaurants Neighborhood'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($restaurantsNeighborhood['RestaurantsNeighborhood']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Restaurant Id'); ?></dt>
		<dd>
			<?php echo h($restaurantsNeighborhood['RestaurantsNeighborhood']['restaurant_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Neighborhood Id'); ?></dt>
		<dd>
			<?php echo h($restaurantsNeighborhood['RestaurantsNeighborhood']['neighborhood_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Restaurants Neighborhood'), array('action' => 'edit', $restaurantsNeighborhood['RestaurantsNeighborhood']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Restaurants Neighborhood'), array('action' => 'delete', $restaurantsNeighborhood['RestaurantsNeighborhood']['id']), null, __('Are you sure you want to delete # %s?', $restaurantsNeighborhood['RestaurantsNeighborhood']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Restaurants Neighborhoods'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Restaurants Neighborhood'), array('action' => 'add')); ?> </li>
	</ul>
</div>
