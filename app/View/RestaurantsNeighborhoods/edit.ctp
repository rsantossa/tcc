<div class="restaurantsNeighborhoods form">
<?php echo $this->Form->create('RestaurantsNeighborhood'); ?>
	<fieldset>
		<legend><?php echo __('Edit Restaurants Neighborhood'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('restaurant_id');
		echo $this->Form->input('neighborhood_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('RestaurantsNeighborhood.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('RestaurantsNeighborhood.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Restaurants Neighborhoods'), array('action' => 'index')); ?></li>
	</ul>
</div>
