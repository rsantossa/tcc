<div class="restaurantsNeighborhoods form">
<?php echo $this->Form->create('RestaurantsNeighborhood'); ?>
	<fieldset>
		<legend><?php echo __('Add Restaurants Neighborhood'); ?></legend>
	<?php
		echo $this->Form->input('restaurant_id');
		echo $this->Form->input('neighborhood_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Restaurants Neighborhoods'), array('action' => 'index')); ?></li>
	</ul>
</div>
