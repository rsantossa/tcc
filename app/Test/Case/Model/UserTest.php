<?php
App::uses('User', 'Model');

/**
 * User Test Case
 *
 */
class UserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user',
		'app.role',
		'app.activity',
		'app.authentication',
		'app.order',
		'app.restaurant',
		'app.restaurant_rating',
		'app.restaurant_review',
		'app.cuisine',
		'app.restaurants_cuisine',
		'app.neighborhood',
		'app.restaurants_neighborhood',
		'app.product',
		'app.product_rating',
		'app.order_product',
		'app.restaurants_product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->User = ClassRegistry::init('User');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->User);

		parent::tearDown();
	}

}
