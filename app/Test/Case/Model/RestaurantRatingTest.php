<?php
App::uses('RestaurantRating', 'Model');

/**
 * RestaurantRating Test Case
 *
 */
class RestaurantRatingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.restaurant_rating',
		'app.restaurant',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RestaurantRating = ClassRegistry::init('RestaurantRating');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RestaurantRating);

		parent::tearDown();
	}

}
