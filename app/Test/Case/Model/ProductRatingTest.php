<?php
App::uses('ProductRating', 'Model');

/**
 * ProductRating Test Case
 *
 */
class ProductRatingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product_rating',
		'app.user',
		'app.product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProductRating = ClassRegistry::init('ProductRating');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProductRating);

		parent::tearDown();
	}

}
