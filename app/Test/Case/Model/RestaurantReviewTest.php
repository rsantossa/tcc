<?php
App::uses('RestaurantReview', 'Model');

/**
 * RestaurantReview Test Case
 *
 */
class RestaurantReviewTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.restaurant_review',
		'app.user',
		'app.restaurant'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RestaurantReview = ClassRegistry::init('RestaurantReview');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RestaurantReview);

		parent::tearDown();
	}

}
