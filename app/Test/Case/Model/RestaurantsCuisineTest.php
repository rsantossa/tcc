<?php
App::uses('RestaurantsCuisine', 'Model');

/**
 * RestaurantsCuisine Test Case
 *
 */
class RestaurantsCuisineTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.restaurants_cuisine',
		'app.restaurant',
		'app.user',
		'app.order',
		'app.order_product',
		'app.product',
		'app.product_rating',
		'app.restaurants_product',
		'app.restaurant_rating',
		'app.restaurant_review',
		'app.cuisine',
		'app.neighborhood',
		'app.restaurants_neighborhood'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RestaurantsCuisine = ClassRegistry::init('RestaurantsCuisine');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RestaurantsCuisine);

		parent::tearDown();
	}

}
