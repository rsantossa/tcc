<?php
App::uses('RestaurantsProduct', 'Model');

/**
 * RestaurantsProduct Test Case
 *
 */
class RestaurantsProductTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.restaurants_product',
		'app.restaurant',
		'app.user',
		'app.order',
		'app.order_product',
		'app.product',
		'app.product_rating',
		'app.restaurant_rating',
		'app.restaurant_review',
		'app.cuisine',
		'app.restaurants_cuisine',
		'app.neighborhood',
		'app.restaurants_neighborhood'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RestaurantsProduct = ClassRegistry::init('RestaurantsProduct');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RestaurantsProduct);

		parent::tearDown();
	}

}
