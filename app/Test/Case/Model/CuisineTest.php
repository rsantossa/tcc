<?php
App::uses('Cuisine', 'Model');

/**
 * Cuisine Test Case
 *
 */
class CuisineTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.cuisine',
		'app.restaurant',
		'app.restaurants_cuisine'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Cuisine = ClassRegistry::init('Cuisine');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Cuisine);

		parent::tearDown();
	}

}
