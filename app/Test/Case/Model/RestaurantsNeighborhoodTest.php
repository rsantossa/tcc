<?php
App::uses('RestaurantsNeighborhood', 'Model');

/**
 * RestaurantsNeighborhood Test Case
 *
 */
class RestaurantsNeighborhoodTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.restaurants_neighborhood',
		'app.restaurant',
		'app.user',
		'app.order',
		'app.order_product',
		'app.product',
		'app.product_rating',
		'app.restaurants_product',
		'app.restaurant_rating',
		'app.restaurant_review',
		'app.cuisine',
		'app.restaurants_cuisine',
		'app.neighborhood'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RestaurantsNeighborhood = ClassRegistry::init('RestaurantsNeighborhood');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RestaurantsNeighborhood);

		parent::tearDown();
	}

}
