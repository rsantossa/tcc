<?php
App::uses('RestaurantRatingsController', 'Controller');

/**
 * RestaurantRatingsController Test Case
 *
 */
class RestaurantRatingsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.restaurant_rating',
		'app.restaurant',
		'app.user',
		'app.role',
		'app.activity',
		'app.authentication',
		'app.order',
		'app.order_product',
		'app.product',
		'app.product_rating',
		'app.restaurants_product',
		'app.restaurant_review',
		'app.cuisine',
		'app.restaurants_cuisine',
		'app.neighborhood',
		'app.restaurants_neighborhood'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

}
