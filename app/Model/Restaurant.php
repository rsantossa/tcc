<?php
App::uses('AppModel', 'Model');
/**
 * Restaurant Model
 *
 * @property User $User
 * @property Order $Order
 * @property RestaurantRating $RestaurantRating
 * @property RestaurantReview $RestaurantReview
 * @property Cuisine $Cuisine
 * @property Neighborhood $Neighborhood
 * @property Product $Product
 */
class Restaurant extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'restaurant_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'RestaurantRating' => array(
			'className' => 'RestaurantRating',
			'foreignKey' => 'restaurant_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'RestaurantReview' => array(
			'className' => 'RestaurantReview',
			'foreignKey' => 'restaurant_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => 'RestaurantReview.created DESC',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Cuisine' => array(
			'className' => 'Cuisine',
			'joinTable' => 'restaurants_cuisines',
			'foreignKey' => 'restaurant_id',
			'associationForeignKey' => 'cuisine_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Neighborhood' => array(
			'className' => 'Neighborhood',
			'joinTable' => 'restaurants_neighborhoods',
			'foreignKey' => 'restaurant_id',
			'associationForeignKey' => 'neighborhood_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Product' => array(
			'className' => 'Product',
			'joinTable' => 'restaurants_products',
			'foreignKey' => 'restaurant_id',
			'associationForeignKey' => 'product_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

	public function afterSave(){
		if( isset($this->id) )
			$this->updateAttachments($this->id);
	}

	public function updateAttachments($id){
		$uploadPath = WWW_ROOT.'files'.DS.'Restaurant'.DS;
		$files = glob($uploadPath.'temp'.DS.'*.jpg');

		if( count($files) )
			foreach($files as $file){
				if(!is_dir($uploadPath.$id))
					@mkdir($uploadPath.$id);
				@rename($file, str_replace('temp', $id, $file));
			}

		return true;
	}

	public function getRatings($restaurant_id){
		$likes = $this->RestaurantRating->find('count', array(
			'conditions' => array(
				'Restaurant.id' => $restaurant_id,
				'RestaurantRating.likes' => 1
				)
			));

		$dislikes = $this->RestaurantRating->find('count', array(
			'conditions' => array(
				'Restaurant.id' => $restaurant_id,
				'RestaurantRating.dislikes' => 1
				)
			));

		return array('likes' => $likes, 'dislikes' => $dislikes);
	}

	public function getByProduct($product_id){
		return $this->find('first', array(
			'joins' => array(
				array(
					'table' => 'restaurants_products',
					'alias' => 'RestaurantsProduct',
					'type'  => 'LEFT',
					'conditions' => array(
						'RestaurantsProduct.restaurant_id = Restaurant.id'
					)
				),
				array(
					'table' => 'products',
					'alias' => '_Product',
					'type'  => 'LEFT',
					'conditions' => array(
						'_Product.id = RestaurantsProduct.product_id'
					)
				),
			),
			'conditions' => array(
				'_Product.id' => $product_id
			),
			'recursive' => -1
		));
	}

}
