<?php
App::uses('AppModel', 'Model');
/**
 * Cuisine Model
 *
 * @property Restaurant $Restaurant
 */
class Cuisine extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Restaurant' => array(
			'className' => 'Restaurant',
			'joinTable' => 'restaurants_cuisines',
			'foreignKey' => 'cuisine_id',
			'associationForeignKey' => 'restaurant_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
