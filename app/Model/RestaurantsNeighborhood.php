<?php
App::uses('AppModel', 'Model');
/**
 * RestaurantsNeighborhood Model
 *
 * @property Restaurant $Restaurant
 * @property Neighborhood $Neighborhood
 */
class RestaurantsNeighborhood extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Restaurant' => array(
			'className' => 'Restaurant',
			'foreignKey' => 'restaurant_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Neighborhood' => array(
			'className' => 'Neighborhood',
			'foreignKey' => 'neighborhood_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
