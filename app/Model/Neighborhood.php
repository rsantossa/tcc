<?php
App::uses('AppModel', 'Model');
/**
 * Neighborhood Model
 *
 * @property Restaurant $Restaurant
 */
class Neighborhood extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Restaurant' => array(
			'className' => 'Restaurant',
			'joinTable' => 'restaurants_neighborhoods',
			'foreignKey' => 'neighborhood_id',
			'associationForeignKey' => 'restaurant_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

	public $hasMany = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'neighborhood_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Address' => array(
            'className' => 'Address',
            'foreignKey' => 'neighborhood_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
	);

    public function getEnderecoByCep($cep = null){
        if($cep == null)
            return null;

        return $this->Address->find('first', array(
            'conditions' => array('zipcode' => $cep)
            ));
    }

}
