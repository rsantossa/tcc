<?php
App::uses('AppModel', 'Model');
/**
 * Product Model
 *
 * @property ProductRating $ProductRating
 * @property Order $Order
 * @property Restaurant $Restaurant
 */
class Product extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ProductRating' => array(
			'className' => 'ProductRating',
			'foreignKey' => 'product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Order' => array(
			'className' => 'Order',
			'joinTable' => 'order_products',
			'foreignKey' => 'product_id',
			'associationForeignKey' => 'order_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Restaurant' => array(
			'className' => 'Restaurant',
			'joinTable' => 'restaurants_products',
			'foreignKey' => 'product_id',
			'associationForeignKey' => 'restaurant_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

	public function afterSave(){
		if( isset($this->id) )
			$this->updateAttachments($this->id);
	}

	public function updateAttachments($id){
		$uploadPath = WWW_ROOT.'files'.DS.'Product'.DS;
		$files = glob($uploadPath.'temp'.DS.'*.jpg');

		if( count($files) )
			foreach($files as $file){
				if(!is_dir($uploadPath.$id))
					@mkdir($uploadPath.$id);
				@rename($file, str_replace('temp', $id, $file));
			}

		return true;
	}

	public function getOffers(){
		return $this->find('all', array(
			'limit' => 3,
			'conditions' => array(
				'Product.active' => 1
			),
			'order' => 'rand()',
			'recursive' => -1
		));
	}

	public function getRatings($product_id){
		$likes = $this->ProductRating->find('count', array(
			'conditions' => array(
				'Product.id' => $product_id,
				'ProductRating.likes' => 1
				)
			));

		$dislikes = $this->ProductRating->find('count', array(
			'conditions' => array(
				'Product.id' => $product_id,
				'ProductRating.dislikes' => 1
				)
			));

		return array('likes' => $likes, 'dislikes' => $dislikes);
	}
}
