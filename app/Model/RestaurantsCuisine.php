<?php
App::uses('AppModel', 'Model');
/**
 * RestaurantsCuisine Model
 *
 * @property Restaurant $Restaurant
 * @property Cuisine $Cuisine
 */
class RestaurantsCuisine extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Restaurant' => array(
			'className' => 'Restaurant',
			'foreignKey' => 'restaurant_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cuisine' => array(
			'className' => 'Cuisine',
			'foreignKey' => 'cuisine_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
