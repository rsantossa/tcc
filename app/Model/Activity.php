<?php
App::uses('AppModel', 'Model');
App::import('Component', 'SessionComponent');

/**
 * Activity Model
 *
 * @property User $User
 */
class Activity extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getActivities($limit = 5, $providers_uids){
		$order = 'Activity.created DESC';

		return $this->find('all', array(
			'order' => $order,
			'limit' => $limit,
			'conditions' => array(
				'User.provider_uid' => $providers_uids
				)
			));
	}
}
