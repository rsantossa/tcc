<?php
App::uses('AuthComponent', 'Controller/Component');
class User extends AppModel {
    public $name = 'User';

    public $belongsTo = array(
        'Role' => array(
            'className' => 'Role',
            'foreignKey' => 'role_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Neighborhood' => array(
            'className' => 'Neighborhood',
            'foreignKey' => 'neighborhood_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public $hasMany = array(
        'Activity' => array(
            'className' => 'Activity',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Order' => array(
            'className' => 'Order',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ProductRating' => array(
            'className' => 'ProductRating',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'RestaurantRating' => array(
            'className' => 'RestaurantRating',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'RestaurantReview' => array(
            'className' => 'RestaurantReview',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Restaurant' => array(
            'className' => 'Restaurant',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    //para recarregar pedidos, favoritos, etc
    public function loadAssociations(){
        
    }

    public function register( Hybrid_User_Profile $user ){
        $data = array(
            'User' => array(
                'username' => Inflector::slug($user->displayName),
                'name' => $user->displayName,
                'email' => $user->email,
                'role_id' => $this->getRoleId('usuario'),
                'provider' => 'Facebook',
                'provider_uid' => $user->identifier,
                'profile_url' => $user->profileURL,
                'password' => AuthComponent::password('12345')
            )
        );

        $this->create();
        $this->save( $data );

        return $this->read();
    }

    public function getRoleId($slug = null){
        $role = $this->Role->findBySlug($slug);
        return $role['Role']['id'];
    }

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password']) && !isset($this->data[$this->alias]['provider'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;        
    }

}