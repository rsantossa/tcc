/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50509
Source Host           : localhost:3306
Source Database       : tcc

Target Server Type    : MYSQL
Target Server Version : 50509
File Encoding         : 65001

Date: 2012-11-04 23:11:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `activities`
-- ----------------------------
DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `target_type` varchar(100) DEFAULT NULL,
  `target_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log (público) de ações dos usuários ';

-- ----------------------------
-- Records of activities
-- ----------------------------

-- ----------------------------
-- Table structure for `addresses`
-- ----------------------------
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `neighborhood_id` int(11) unsigned NOT NULL,
  `zipcode` varchar(10) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=967531 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `cuisines`
-- ----------------------------
DROP TABLE IF EXISTS `cuisines`;
CREATE TABLE `cuisines` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='Tipos de Cozinha (mineira, italiana, etc)';

-- ----------------------------
-- Records of cuisines
-- ----------------------------
INSERT INTO `cuisines` VALUES ('1', 'Pizzas', 'pizzas');
INSERT INTO `cuisines` VALUES ('2', 'Chinesa', 'chinesa');
INSERT INTO `cuisines` VALUES ('3', 'Japonesa', 'japonesa');
INSERT INTO `cuisines` VALUES ('4', 'Lanches', 'lanches');
INSERT INTO `cuisines` VALUES ('5', 'Italiana', 'italiana');
INSERT INTO `cuisines` VALUES ('6', 'Árabe', 'arabe');
INSERT INTO `cuisines` VALUES ('7', 'Alemã', 'alema');
INSERT INTO `cuisines` VALUES ('8', 'Argentina', 'argentina');
INSERT INTO `cuisines` VALUES ('9', 'Bebidas', 'bebidas');
INSERT INTO `cuisines` VALUES ('10', 'Brasileira', 'brasileira');
INSERT INTO `cuisines` VALUES ('11', 'Carnes', 'carnes');
INSERT INTO `cuisines` VALUES ('12', 'Congelados', 'congelados');
INSERT INTO `cuisines` VALUES ('13', 'Delicatessen', 'delicatessen');
INSERT INTO `cuisines` VALUES ('14', 'Docerias', 'docerias');
INSERT INTO `cuisines` VALUES ('15', 'Espanhola', 'espanhola');
INSERT INTO `cuisines` VALUES ('16', 'Frutos do Mar', 'frutos-do-mar');
INSERT INTO `cuisines` VALUES ('17', 'Galeteria', 'galeteria');
INSERT INTO `cuisines` VALUES ('18', 'Indiana', 'indiana');
INSERT INTO `cuisines` VALUES ('19', 'Mexicana', 'mexicana');
INSERT INTO `cuisines` VALUES ('20', 'Mineira', 'mineira');
INSERT INTO `cuisines` VALUES ('21', 'Natural', 'natural');
INSERT INTO `cuisines` VALUES ('22', 'Portuguesa', 'portuguesa');
INSERT INTO `cuisines` VALUES ('23', 'Saladas', 'saladas');
INSERT INTO `cuisines` VALUES ('24', 'Salgados', 'salgados');
INSERT INTO `cuisines` VALUES ('25', 'Sorvetes', 'sorvetes');
INSERT INTO `cuisines` VALUES ('26', 'Variada', 'variada');
INSERT INTO `cuisines` VALUES ('27', 'Vegetariana', 'vegetariana');

-- ----------------------------
-- Table structure for `neighborhoods`
-- ----------------------------
DROP TABLE IF EXISTS `neighborhoods`;
CREATE TABLE `neighborhoods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `city` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55329 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of neighborhoods
-- ----------------------------
INSERT INTO `neighborhoods` VALUES ('25218', 'Jardim Nossa Senhora Aparecida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25219', 'Água Branca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25220', 'Água Fria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25221', 'Água Funda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25222', 'Água Rasa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25223', 'Alto da Lapa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25224', 'Alto da Mooca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25225', 'Alto da Riviera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25226', 'Alto de Pinheiros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25227', 'Alto do Pari', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25228', 'Altos de Vila Prudente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25229', 'Americanópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25230', 'Anhanguera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25231', 'Aricanduva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25232', 'Artur Alvim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25234', 'Balneário Dom Carlos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25235', 'Balneário Mar Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25236', 'Balneário São Francisco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25237', 'Balneário São José', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25238', 'Baronesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25239', 'Barra Funda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25240', 'Barragem', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25241', 'Barro Branco (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25242', 'Bela Aliança', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25243', 'Bela Vista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25244', 'Belém', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25245', 'Belenzinho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25246', 'Boaçava', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25247', 'Bom Retiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25248', 'Bororé', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25249', 'Bortolândia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25250', 'Bosque da Saúde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25251', 'Brás', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25252', 'Brasilândia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25253', 'Brooklin Novo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25254', 'Brooklin Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25255', 'Burgo Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25256', 'Butantã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25257', 'Cachoeirinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25258', 'Cambuci', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25259', 'Campininha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25260', 'Campo Belo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25261', 'Campo Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25262', 'Campo Limpo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25263', 'Campos Elíseos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25264', 'Cangaíba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25265', 'Canindé', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25266', 'Cantareira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25267', 'Capão do Embira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25268', 'Capão Redondo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25269', 'Capela do Socorro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25270', 'Carandiru', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25271', 'Carrão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25272', 'Casa Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25273', 'Casa Verde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25274', 'Casa Verde Alta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25275', 'Casa Verde Baixa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25276', 'Casa Verde Média', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25277', 'Catumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25278', 'Caxingui', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25279', 'Centro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25280', 'Cerqueira César', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25281', 'Chácara Bandeirantes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25282', 'Chácara Belenzinho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25283', 'Chácara Bosque do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25284', 'Chácara Califórnia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25285', 'Chácara Cocaia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25286', 'Chácara Cruzeiro do Sul', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25287', 'Chácara da Enseada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25288', 'Chácara das Corujas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25290', 'Chácara das Palmeiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25291', 'Chácara do Encosto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25292', 'Chácara Dona Olívia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25293', 'Chácara Figueira Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25294', 'Chácara Flora', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25295', 'Chácara Flórida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25296', 'Chácara Gaivotas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25297', 'Chácara Inglesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25298', 'Chácara Itaim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25299', 'Chácara Japonesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25300', 'Chácara Jaraguá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25301', 'Chácara Mafalda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25302', 'Chácara Maria Trindade', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25303', 'Chácara Meyer', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25304', 'Chácara Monte Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25305', 'Chácara Monte Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25306', 'Chácara Morro Alto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25307', 'Chácara Nani', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25308', 'Chácara Nossa Senhora Aparecida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25309', 'Chácara Nossa Senhora do Bom Conselho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25310', 'Chácara Paiva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25311', 'Chácara Pirajussara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25312', 'Chácara Pouso Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25313', 'Chácara Santa Etelvina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25314', 'Chácara Santa Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25315', 'Chácara Santana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25316', 'Chácara Santo Amaro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25317', 'Chácara Santo Antônio (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25318', 'Chácara Santo Hubertus', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25319', 'Chácara São João', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25320', 'Chácara São Luís', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25321', 'Chácara São Silvestre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25322', 'Chácara Seis de Outubro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25323', 'Chácara Sonho Azul', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25324', 'Chácara Tatuapé', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25325', 'Chácara Três Meninas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25326', 'Chácara Vista Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25327', 'Chácara Vovo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25328', 'Chapada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25329', 'Chora Menino', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25330', 'Cidade Ademar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25331', 'Cidade Antônio Estevão de Carvalho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25332', 'Cidade Auxiliadora', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25333', 'Cidade Centenário', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25334', 'Cidade Continental', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25335', 'Cidade D\'Abril', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25336', 'Cidade Domitila', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25337', 'Cidade dos Bandeirantes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25338', 'Cidade Dutra', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25339', 'Cidade Fim de Semana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25340', 'Cidade Ipava', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25341', 'Cidade Jardim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25342', 'Cidade Júlia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25343', 'Cidade Kemel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25344', 'Cidade Leonor', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25345', 'Cidade Líder', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25346', 'Cidade Mãe do Céu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25347', 'Cidade Monções', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25348', 'Cidade Munhoz Junior', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25349', 'Cidade Nautica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25350', 'Cidade Nitro Operária', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25351', 'Cidade Nitro Química', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25352', 'Cidade Nova Heliópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25353', 'Cidade Nova São Miguel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25354', 'Cidade Patriarca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25355', 'Cidade Popular', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25356', 'Cidade São Francisco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25357', 'Cidade São Mateus', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25358', 'Cidade São Miguel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25359', 'Cidade Satelite', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25360', 'Cidade Satélite Santa Bárbara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25361', 'Cidade Tiradentes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25362', 'Cidade Vargas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25363', 'Cipó do Meio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25364', 'City América', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25365', 'Clube Campestre de São Paulo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25367', 'Clube dos Funcionários Públicos Colônia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25368', 'Colônia (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25369', 'Conjunto dos Bancários', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25370', 'Conjunto Habitacional A E Carvalho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25371', 'Conjunto Habitacional Águia de Haia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25372', 'Conjunto Habitacional Barreira Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25373', 'Conjunto Habitacional Barro Branco I', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25374', 'Conjunto Habitacional Barro Branco II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25375', 'Conjunto Habitacional Brigadeiro Eduardo Gomes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25376', 'Conjunto Habitacional Brigadeiro Faria Lima', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25377', 'Conjunto Habitacional Castro Alves', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25378', 'Conjunto Habitacional Fazenda do Carmo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25379', 'Conjunto Habitacional Inácio Monteiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25380', 'Conjunto Habitacional Instituto Adventista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25382', 'Conjunto Habitacional Jardim São Bento', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25383', 'Conjunto Habitacional Jardim São Francisco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25384', 'Conjunto Habitacional Jova Rural', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25385', 'Conjunto Habitacional Juscelino Kubitschek', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25386', 'Conjunto Habitacional Marechal Mascarenhas de Morais', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25387', 'Conjunto Habitacional Padre José de Anchieta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25388', 'Conjunto Habitacional Padre Manoel da Nóbrega', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25389', 'Conjunto Habitacional Padre Manoel de Paiva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25390', 'Conjunto Habitacional Pirajussara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25391', 'Conjunto Habitacional Recanto dos Humildes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25392', 'Conjunto Habitacional Santa Etelvina I', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25393', 'Conjunto Habitacional Santa Etelvina II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25394', 'Conjunto Habitacional Santa Etelvina III', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25395', 'Conjunto Habitacional Sitio Caraguata', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25396', 'Conjunto Habitacional Sitio Conceição', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25397', 'Conjunto Habitacional Teotonio Vilela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25398', 'Conjunto Habitacional Vila Nova Cachoeirinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25399', 'Conjunto Promorar Estrada da Parada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25400', 'Conjunto Promorar Raposo Tavares', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25401', 'Conjunto Promorar Rio Claro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25402', 'Conjunto Promorar São Luis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25403', 'Conjunto Promorar Sapopemba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25404', 'Conjunto Promorar Vila Maria III', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25405', 'Conjunto Residencial Alpes do Jaraguá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25406', 'Conjunto Residencial Bandeirantes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25407', 'Conjunto Residencial Butantã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25408', 'Conjunto Residencial Elisio Teixeira Leite', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25409', 'Conjunto Residencial Ingai', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25410', 'Conjunto Residencial Jardim Canaã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25411', 'Conjunto Residencial José Bonifácio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25412', 'Conjunto Residencial Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25413', 'Conjunto Residencial Novo Pacaembu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25414', 'Conjunto Residencial Paraíso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25415', 'Conjunto Residencial Pinheirinho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25416', 'Conjunto Residencial Prestes Maia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25417', 'Conjunto Residencial Salvador Tolezani', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25418', 'Conjunto Residencial Santa Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25419', 'Conjunto Residencial Santa Terezinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25420', 'Conjunto Residencial Sitio Oratório', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25421', 'Conjunto Residencial Vista Verde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25422', 'Consolação', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25423', 'Copacabana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25425', 'Cursino', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25426', 'Educandário', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25427', 'Eldorado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25428', 'Embu Mirim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25429', 'Embura', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25430', 'Engenheiro Goulart', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25431', 'Engenheiro Marsilac', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25432', 'Engenheiro Trindade', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25433', 'Ermelino Matarazzo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25434', 'Estância Jaraguá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25435', 'Estância Mirim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25436', 'Estância Tangara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25437', 'Vila Nova Jaguaré', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25438', 'Fazenda Aricanduva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25439', 'Fazenda Itaim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25440', 'Fazenda da Juta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25441', 'Fazenda Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25442', 'Ferreira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25443', 'Freguesia do Ó', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25444', 'Furnas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25445', 'Gleba do Pêssego', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25446', 'Grajaú', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25447', 'Granja Julieta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25448', 'Granja Nossa Senhora Aparecida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25449', 'Guacuri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25450', 'Guaianazes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25451', 'Guaiaúna', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25452', 'Guapira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25453', 'Guarapiranga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25454', 'Guavirutuba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25455', 'Higienópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25456', 'Horto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25457', 'Horto Florestal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25458', 'Ibirapuera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25459', 'Iguatemi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25460', 'Imirim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25461', 'Indianópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25462', 'Instituto de Previdência', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25463', 'Interlagos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25464', 'Ipiranga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25465', 'Itaberaba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25466', 'Itaim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25467', 'Itaim Bibi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25468', 'Itaim Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25469', 'Itaquera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25470', 'Itupu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25471', 'Jabaquara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25472', 'Jaçanã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25473', 'Jaguaré', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25474', 'Jaraguá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25475', 'Jardim Adelaide', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25476', 'Jardim Adelfiore', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25477', 'Jardim Adélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25478', 'Jardim Adhemar de Barros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25479', 'Jardim Adutora', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25480', 'Jardim Aeroporto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25481', 'Jardim Aida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25482', 'Jardim Aimoré', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25483', 'Jardim Aladim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25485', 'Jardim Alfredo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25486', 'Jardim Almanara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25487', 'Jardim Almeida Prado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25488', 'Jardim Alpino', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25489', 'Jardim Alteza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25490', 'Jardim Alto Alegre (São Rafael)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25491', 'Jardim Alto do Rio Bonito', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25492', 'Jardim Alto Pedroso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25493', 'Jardim Alva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25494', 'Jardim Alvarenga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25495', 'Jardim Alvina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25496', 'Jardim Alviverde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25497', 'Jardim Alvorada (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25498', 'Jardim Alzira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25499', 'Jardim Amália', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25500', 'Jardim Amaralina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25501', 'Jardim América', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25502', 'Jardim América da Penha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25503', 'Jardim Ampliação', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25504', 'Jardim Ana Lúcia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25505', 'Jardim Ana Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25506', 'Jardim Ana Rosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25507', 'Jardim Anália Franco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25508', 'Jardim Anchieta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25509', 'Jardim Andaraí', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25510', 'Jardim Ângela (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25511', 'Jardim Ângela (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25512', 'Jardim Anhanguera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25513', 'Jardim Antártica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25514', 'Jardim Aparecida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25515', 'Jardim Apurá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25516', 'Jardim Aracati', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25517', 'Jardim Arco-Iris', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25518', 'Jardim Aricanduva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25519', 'Jardim Arize', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25520', 'Jardim Arizona', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25521', 'Jardim Arpoador', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25522', 'Jardim Artur Alvim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25523', 'Jardim Assunção', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25524', 'Jardim Ataliba Leonel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25525', 'Jardim Atibaia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25526', 'Jardim Atlântico', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25527', 'Jardim Augusta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25528', 'Jardim Augusto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25529', 'Jardim Aurélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25530', 'Jardim Aurélio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25531', 'Jardim Aurora (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25532', 'Jardim Avelino', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25533', 'Jardim Avenida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25534', 'Jardim Bandeirante', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25535', 'Jardim Bandeirantes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25536', 'Jardim Barros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25537', 'Jardim Bartira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25538', 'Jardim Batalha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25539', 'Jardim Beatriz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25540', 'Jardim Bela Vista (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25541', 'Jardim Belaura', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25542', 'Jardim Belcito', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25543', 'Jardim Belém', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25544', 'Jardim Bélgica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25545', 'Jardim Benfica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25546', 'Jardim Bibi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25547', 'Jardim Bichinhos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25548', 'Jardim Boa Esperança', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25549', 'Jardim Boa Sorte', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25550', 'Jardim Boa Vista (Zona Oeste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25551', 'Jardim Bom Clima', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25552', 'Jardim Bom Pastor', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25553', 'Jardim Bom Refúgio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25554', 'Jardim Bonfiglioli', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25555', 'Jardim Bonito', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25556', 'Jardim Borba Gato', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25557', 'Jardim Bosque do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25558', 'Jardim Botucatu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25559', 'Jardim Brasil (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25560', 'Jardim Brasília (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25561', 'Jardim Britânia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25562', 'Jardim Bronzato', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25563', 'Jardim Buriti', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25564', 'Jardim Caboré', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25565', 'Jardim Cabuçu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25566', 'Jardim Cachoeira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25567', 'Jardim Caguassu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25568', 'Jardim Camara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25569', 'Jardim Camargo Novo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25570', 'Jardim Cambara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25571', 'Jardim Campinas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25572', 'Jardim Campo dos Ferreiros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25573', 'Jardim Campo Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25574', 'Jardim Campo Limpo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25575', 'Jardim Campos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25576', 'Jardim Capão Redondo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25577', 'Jardim Capela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25578', 'Jardim Capelinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25579', 'Jardim Caravelas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25580', 'Jardim Carlu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25581', 'Jardim Carolina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25582', 'Jardim Carombé', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25583', 'Jardim Casa Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25584', 'Jardim Casa Pintada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25585', 'Jardim Casablanca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25586', 'Jardim Castelo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25587', 'Jardim Catanduva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25588', 'Jardim Catarina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25589', 'Jardim Cecy', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25591', 'Jardim Cedro do Líbano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25592', 'Jardim Celeste', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25593', 'Jardim Célia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25594', 'Jardim Centenário', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25595', 'Jardim Central', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25596', 'Jardim Cercado da Estrada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25597', 'Jardim Chester', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25598', 'Jardim Cibele', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25599', 'Jardim Cidade Pirituba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25601', 'Jardim Cimobil', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25602', 'Jardim Cinco de Julho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25603', 'Jardim Clara Regina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25604', 'Jardim Clarice', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25605', 'Jardim Cláudia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25606', 'Jardim Cleide', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25607', 'Jardim Célia (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25608', 'Jardim Clímax', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25609', 'Jardim Cliper', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25610', 'Jardim Coimbra', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25612', 'Jardim Colina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25613', 'Jardim Colombo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25614', 'Jardim Colonial', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25615', 'Jardim Colorado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25616', 'Jardim Comercial', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25617', 'Jardim Concórdia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25618', 'Jardim Consórcio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25619', 'Jardim Consupler', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25620', 'Jardim Copacabana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25621', 'Jardim Cordeiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25622', 'Jardim Cotching', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25623', 'Jardim Cotiana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25624', 'Jardim Cotinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25625', 'Jardim Cristal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25626', 'Jardim Cristália', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25627', 'Jardim Cristina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25628', 'Jardim Cruzeiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25630', 'Jardim D\'Abril', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25631', 'Jardim da Campina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25632', 'Jardim da Conquista (Zona Oeste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25633', 'Jardim da Glória', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25635', 'Jardim da Saúde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25636', 'Jardim Dalmo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25637', 'Jardim Damasceno', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25638', 'Jardim Danfer', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25639', 'Jardim das Acácias', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25640', 'Jardim das Bandeiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25641', 'Jardim das Camélias', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25642', 'Jardim das Esmeraldas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25643', 'Jardim das Flores', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25644', 'Jardim das Graças', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25645', 'Jardim das Imbuias', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25646', 'Jardim das Laranjeiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25647', 'Jardim das Oliveiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25648', 'Jardim das Palmas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25649', 'Jardim das Palmeiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25650', 'Jardim das Pedras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25651', 'Jardim das Praias', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25652', 'Jardim das Rosas (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25653', 'Jardim das Vertentes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25654', 'Jardim Daysy', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25655', 'Jardim de Lorenzo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25656', 'Jardim Denyse', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25657', 'Jardim Deolinda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25658', 'Jardim Dinorah', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25659', 'Jardim Diomar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25660', 'Jardim Dionisio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25661', 'Jardim do Alto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25662', 'Jardim do Campo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25663', 'Jardim do Carmo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25664', 'Jardim do Centro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25665', 'Jardim do Colégio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25666', 'Jardim do Divino', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25667', 'Jardim do Lago', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25668', 'Jardim do Russo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25669', 'Jardim do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25670', 'Jardim do Tiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25671', 'Jardim Dom Bosco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25672', 'Jardim Dom Fernando', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25673', 'Jardim Dom José', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25674', 'Jardim Domitila', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25675', 'Jardim Dona Deolinda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25676', 'Jardim Dona Sinhá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25677', 'Jardim Donária', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25678', 'Jardim dos Álamos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25679', 'Jardim dos Bichinhos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25680', 'Jardim dos Cataldis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25681', 'Jardim dos Estados', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25682', 'Jardim dos Eucaliptos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25683', 'Jardim dos Francos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25684', 'Jardim dos Ipês', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25685', 'Jardim dos Jacarandás', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25686', 'Jardim dos Lagos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25687', 'Jardim dos Manacás', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25688', 'Jardim dos Pinheiros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25689', 'Jardim dos Prados', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25690', 'Jardim Dracena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25691', 'Jardim Drumond', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25692', 'Jardim Duprat', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25693', 'Jardim Edda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25694', 'Jardim Edi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25695', 'Jardim Edilene', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25696', 'Jardim Edith', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25697', 'Jardim Edwiges', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25698', 'Jardim Egle', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25699', 'Jardim Elba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25700', 'Jardim Eledy', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25701', 'Jardim Eliana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25702', 'Jardim Eliane', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25703', 'Jardim Elisa Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25704', 'Jardim Elisio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25705', 'Jardim Eliza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25706', 'Jardim Elizabeth', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25707', 'Jardim Ellus', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25708', 'Jardim Elza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25709', 'Jardim Emilia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25710', 'Jardim Engenheiro Visconti', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25711', 'Jardim Ernestina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25712', 'Jardim Esmeralda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25713', 'Jardim Esperança', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25714', 'Jardim Ester', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25715', 'Jardim Ester Yolanda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25716', 'Jardim Estrela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25717', 'Jardim Etelvina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25718', 'Jardim Europa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25719', 'Jardim Eva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25720', 'Jardim Everest', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25721', 'Jardim Faria Lima', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25722', 'Jardim Felicidade (Zona Oeste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25723', 'Jardim Fernandes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25724', 'Jardim Figueira Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25725', 'Jardim Fim de Semana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25726', 'Jardim Flamingo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25727', 'Jardim Floresta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25728', 'Jardim Flórida Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25729', 'Jardim Fluminense', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25730', 'Jardim Fonte do Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25731', 'Jardim Fonte São Miguel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25732', 'Jardim Franca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25733', 'Jardim Francisco Mentem', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25734', 'Jardim Franganielo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25735', 'Jardim Fraternidade', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25736', 'Jardim Fujihara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25737', 'Jardim Galli', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25738', 'Jardim Germânia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25739', 'Jardim Gianetti', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25740', 'Jardim Gilberto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25741', 'Jardim Gilda Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25742', 'Jardim Glória', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25743', 'Jardim Gonzaga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25744', 'Jardim Grimaldi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25745', 'Jardim Guacuri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25746', 'Jardim Guaianazes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25747', 'Jardim Guairaca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25748', 'Jardim Guanabara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25749', 'Jardim Guanca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25750', 'Jardim Guanhembu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25751', 'Jardim Guapira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25752', 'Jardim Guarani', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25753', 'Jardim Guarapiranga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25754', 'Jardim Guarau', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25755', 'Jardim Guarujá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25756', 'Jardim Guedala', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25757', 'Jardim Guiomar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25758', 'Jardim Gustavo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25759', 'Jardim Hadad', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25760', 'Jardim Haia do Carrão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25761', 'Jardim Harmonia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25762', 'Jardim Helena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25763', 'Jardim Helga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25765', 'Jardim Heliomar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25766', 'Jardim Heloisa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25767', 'Jardim Hercilia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25768', 'Jardim Herculano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25769', 'Jardim Herplin', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25770', 'Jardim Hípico', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25771', 'Jardim Horizonte Azul', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25772', 'Jardim Horto Florestal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25773', 'Jardim Humaitá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25774', 'Jardim Humberto Nastari', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25775', 'Jardim Iae', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25776', 'Jardim Iara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25777', 'Jardim Ibirapuera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25778', 'Jardim Ibiratiba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25779', 'Jardim Ibitirama', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25780', 'Jardim Icara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25781', 'Jardim Icaraí', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25782', 'Jardim Ida Guedes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25783', 'Jardim Ideal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25784', 'Jardim Iguatemi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25785', 'Jardim Imbé', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25786', 'Jardim Imperador (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25787', 'Jardim Imperial', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25788', 'Jardim Império', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25789', 'Jardim Indaiá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25790', 'Jardim Independência', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25791', 'Jardim Ingá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25792', 'Jardim Internacional', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25793', 'Jardim Ipanema (São Miguel)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25794', 'Jardim Ipê', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25795', 'Jardim Iporã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25796', 'Jardim Iporanga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25797', 'Jardim Iracema', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25798', 'Jardim Irapiranga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25799', 'Jardim Irene', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25800', 'Jardim Íris', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25801', 'Jardim Itacolomi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25803', 'Jardim Itajai', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25804', 'Jardim Itália', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25805', 'Jardim Itamaraty', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25806', 'Jardim Itaoca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25807', 'Jardim Itapemirim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25808', 'Jardim Itapeva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25809', 'Jardim Itápolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25810', 'Jardim Itapura', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25811', 'Jardim Itatiaia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25812', 'Jardim Itatinga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25813', 'Jardim Iva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25814', 'Jardim Ivana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25815', 'Jardim Ivete', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25816', 'Jardim Ivone', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25817', 'Jardim Jabaquara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25818', 'Jardim Jaçanã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25819', 'Jardim Jamaica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25820', 'Jardim Japão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25821', 'Jardim Jaqueline', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25822', 'Jardim Jaraguá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25823', 'Jardim Jaú (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25824', 'Jardim Jeriva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25825', 'Jardim Joamar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25826', 'Jardim Joana D\'Arc', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25827', 'Jardim João XXIII', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25829', 'Jardim Jordão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25830', 'Jardim Jua', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25831', 'Jardim Julieta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25832', 'Jardim Jurema', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25833', 'Jardim Jussara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25834', 'Jardim Kagohara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25835', 'Jardim Kaj', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25836', 'Jardim Keralux', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25837', 'Jardim Kika', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25838', 'Jardim Kioto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25839', 'Jardim Lajeado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25840', 'Jardim Lallo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25841', 'Jardim Laone', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25842', 'Jardim Lapena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25843', 'Lar São Paulo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25844', 'Jardim Laranjal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25845', 'Jardim Laranjeiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25846', 'Jardim Laura', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25847', 'Jardim Leila', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25848', 'Jardim Leme', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25849', 'Jardim Leni', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25850', 'Jardim Leonardo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25851', 'Jardim Leônidas Moreira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25852', 'Jardim Leonor', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25853', 'Jardim Leonor Mendes de Barros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25854', 'Jardim Letícia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25855', 'Jardim Líbano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25856', 'Jardim Lice', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25857', 'Jardim Lider', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25858', 'Jardim Liderança', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25859', 'Jardim Lídia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25860', 'Jardim Lilah', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25861', 'Jardim Lisboa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25862', 'Jardim Londrina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25863', 'Jardim Los Angeles', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25864', 'Jardim Lourdes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25865', 'Jardim Luanda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25866', 'Jardim Lucélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25867', 'Jardim Lúcia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25868', 'Jardim Luciana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25869', 'Jardim Lucinda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25870', 'Jardim Lúcio de Castro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25871', 'Jardim Lugo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25872', 'Jardim Luísa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25873', 'Jardim Luso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25874', 'Jardim Luzitânia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25875', 'Jardim Mabel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25876', 'Jardim Macedônia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25877', 'Jardim Machado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25878', 'Jardim Maggi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25879', 'Jardim Maia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25880', 'Jardim Mália I', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25881', 'Jardim Mália II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25882', 'Jardim Mangalot', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25883', 'Jardim Marabá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25884', 'Jardim Maracá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25885', 'Jardim Maracanã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25886', 'Jardim Marajoara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25887', 'Jardim Marcel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25888', 'Jardim Marcelo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25889', 'Jardim Marciano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25890', 'Jardim Marco Paulo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25891', 'Jardim Margarida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25892', 'Jardim Maria Amália', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25893', 'Jardim Maria Duarte', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25894', 'Jardim Maria Emília', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25895', 'Jardim Maria Estela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25896', 'Parque Maria Fernandes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25897', 'Jardim Maria Lúcia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25898', 'Jardim Maria Luiza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25899', 'Jardim Maria Margarida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25900', 'Jardim Maria Nazaré', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25901', 'Jardim Maria Rita', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25902', 'Jardim Maria Sampaio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25903', 'Jardim Maria Virginia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25904', 'Jardim Mariane', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25905', 'Jardim Marilda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25906', 'Jardim Marília', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25907', 'Jardim Mariliza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25908', 'Jardim Marilu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25909', 'Jardim Marina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25910', 'Jardim Maringá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25911', 'Jardim Marisa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25912', 'Jardim Maristela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25913', 'Jardim Marpu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25914', 'Jardim Marquesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25915', 'Jardim Martha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25916', 'Jardim Martini', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25917', 'Jardim Martinica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25918', 'Jardim Martins Silva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25919', 'Jardim Mascarenhas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25920', 'Jardim Matarazzo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25921', 'Jardim Mazza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25922', 'Jardim Meliunas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25923', 'Jardim Melo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25924', 'Jardim Mikail', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25925', 'Jardim Mimar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25926', 'Jardim Miragaia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25927', 'Jardim Mirante', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25928', 'Jardim Miriam', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25929', 'Jardim Mitsutani', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25930', 'Jardim Modelo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25931', 'Jardim Mônica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25932', 'Jardim Monjolo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25933', 'Jardim Monte Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25934', 'Jardim Monte Azul', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25935', 'Jardim Monte Belo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25936', 'Jardim Monte Kemel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25937', 'Jardim Morais Prado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25938', 'Jardim Moreno', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25939', 'Jardim Morganti', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25940', 'Jardim Morro Verde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25941', 'Jardim Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25942', 'Jardim Mutinga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25943', 'Jardim Myrna', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25944', 'Jardim Nadir', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25945', 'Jardim Nagib Salem', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25946', 'Jardim Nair', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25947', 'Jardim Nakamura', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25948', 'Jardim Namba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25949', 'Jardim Nardini', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25950', 'Jardim Nastari', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25951', 'Jardim Natal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25952', 'Jardim Naufal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25953', 'Jardim Nazareth', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25954', 'Jardim Neide', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25955', 'Jardim Neila', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25956', 'Jardim Nélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25960', 'Jardim Nelia IV', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25962', 'Jardim Nelly', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25963', 'Jardim Nice', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25964', 'Jardim Niteroi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25965', 'Jardim Nizia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25966', 'Jardim Noemia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25967', 'Jardim Nordeste', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25968', 'Jardim Norma', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25969', 'Jardim Noronha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25970', 'Jardim Nossa Senhora do Carmo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25971', 'Jardim Nosso Lar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25972', 'Jardim Nova Brasília', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25973', 'Jardim Nova Colonia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25974', 'Jardim Nova Germania', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25975', 'Jardim Nova Guaianazes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25976', 'Jardim Nova Tereza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25977', 'Jardim Nove de Julho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25978', 'Jardim Novo Carrão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25979', 'Jardim Novo Horizonte', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25980', 'Jardim Novo Mundo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25981', 'Jardim Novo Parelheiros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25982', 'Jardim Novo Santo Amaro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25983', 'Jardim Novo Taboão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25984', 'Jardim Odete', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25985', 'Jardim Olinda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25986', 'Jardim Olympia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25987', 'Jardim Ondina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25988', 'Jardim Orbam', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25989', 'Jardim Oriental', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25990', 'Jardim Orion', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25991', 'Jardim Orly', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25992', 'Jardim Ormendina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25993', 'Jardim Outono', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25994', 'Jardim Palmares (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25995', 'Jardim Panorama', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25996', 'Jardim Panorama D\'Oeste', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25997', 'Jardim Paquetá (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25998', 'Jardim Pará', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('25999', 'Jardim Paraguaçu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26000', 'Jardim Paris', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26001', 'Jardim Parque Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26002', 'Jardim Patente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26003', 'Jardim Patente Novo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26004', 'Jardim Pauliceia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26005', 'Jardim Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26006', 'Jardim Paulistano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26007', 'Jardim Paulo Afonso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26008', 'Jardim Paulo Borba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26009', 'Jardim Paulo VI', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26010', 'Jardim Pedra Branca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26011', 'Jardim da Pedreira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26012', 'Jardim Pedro José Nunes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26013', 'Jardim Penha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26014', 'Jardim Pereira Leite', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26015', 'Jardim Peri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26016', 'Jardim Peri Novo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26017', 'Jardim Peri Peri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26018', 'Jardim Perus', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26019', 'Jardim Petisco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26020', 'Jardim Petrópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26021', 'Jardim Picolo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26022', 'Jardim Pinheiros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26023', 'Jardim Piqueri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26024', 'Jardim Piqueroby', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26025', 'Jardim Piracema', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26026', 'Jardim Piracuama', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26027', 'Jardim Pirajussara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26028', 'Jardim Piratininga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26029', 'Jardim Pirituba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26030', 'Jardim Planalto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26031', 'Jardim Pongilutti', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26032', 'Jardim Ponte Rasa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26033', 'Jardim Popular', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26034', 'Jardim Porteira Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26035', 'Jardim Pouso Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26037', 'Jardim Presidente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26038', 'Jardim Previdência', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26039', 'Jardim Primavera (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26040', 'Jardim Princesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26041', 'Jardim Progresso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26042', 'Jardim Promissão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26043', 'Jardim Prudência', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26044', 'Jardim Quarto Centenário', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26045', 'Jardim Quisisana (Vila Nova Curuçá)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26046', 'Jardim Ramala', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26047', 'Jardim Raposo Tavares', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26048', 'Jardim Real', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26049', 'Jardim Recanto do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26050', 'Jardim Record', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26051', 'Jardim Recreio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26052', 'Jardim Redenção', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26053', 'Jardim Redil', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26054', 'Jardim Regina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26056', 'Jardim Regis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26057', 'Jardim Reimberg', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26058', 'Jardim Remo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26059', 'Jardim Represa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26060', 'Jardim República', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26061', 'Jardim Ricardo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26062', 'Jardim Rincão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26063', 'Jardim Rio Bonito', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26064', 'Jardim Rio Douro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26065', 'Jardim Rio Pequeno', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26066', 'Jardim Riviera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26067', 'Jardim Rizzo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26068', 'Jardim Robru', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26069', 'Jardim Rodolfo Pirani', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26070', 'Jardim Romano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26071', 'Jardim Roni', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26072', 'Jardim Rosa Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26073', 'Jardim Rosalina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26074', 'Jardim Rosana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26075', 'Jardim Roseli', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26076', 'Jardim Rosider', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26077', 'Jardim Rosina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26078', 'Jardim Rossin', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26079', 'Jardim Rubilene', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26080', 'Jardim Rubio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26081', 'Jardim Russo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26082', 'Jardim Ruth', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26083', 'Jardim S Kemel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26084', 'Jardim Sabará', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26085', 'Jardim Sabiá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26086', 'Jardim Sagrado Coração', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26087', 'Jardim Samambaia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26088', 'Jardim Samara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26089', 'Jardim Samas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26090', 'Jardim Sandra', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26092', 'Jardim Santa Adélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26093', 'Jardim Santa Bárbara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26094', 'Jardim Santa Branca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26095', 'Jardim Santa Cecília', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26096', 'Jardim Santa Cruz (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26097', 'Jardim Santa Edwiges (Grajaú)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26098', 'Jardim Santa Efigênia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26099', 'Jardim Santa Emília', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26100', 'Jardim Santa Etelvina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26101', 'Jardim Santa Fé (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26102', 'Jardim Santa Filomena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26103', 'Jardim Santa Francisca Cabrini', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26104', 'Jardim Santa Gertrudes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26105', 'Jardim Santa Helena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26106', 'Jardim Santa Inês', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26107', 'Jardim Santa Josefina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26108', 'Jardim Santa Lucrécia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26109', 'Jardim Santa Luísa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26110', 'Jardim Santa Marcelina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26111', 'Jardim Santa Margarida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26112', 'Jardim Santa Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26113', 'Jardim Santa Mônica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26114', 'Jardim Santa Rita', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26115', 'Jardim Santa Terezinha (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26116', 'Jardim Santa Tereza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26117', 'Jardim Santa Terezinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26118', 'Jardim Santa Zélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26119', 'Jardim Santana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26120', 'Jardim Santo Alberto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26121', 'Jardim Santo Amaro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26122', 'Jardim Santo André', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26123', 'Jardim Santo Antoninho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26124', 'Jardim Santo Antônio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26125', 'Jardim Santo Elias', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26126', 'Jardim Santo Onofre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26127', 'Jardim Santos Dumont', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26128', 'Jardim São Benedito', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26129', 'Jardim São Bento', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26130', 'Jardim São Bento Novo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26131', 'Jardim São Bernardo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26132', 'Jardim São Carlos (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26133', 'Jardim São Cristóvão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26134', 'Jardim São Domingos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26135', 'Jardim São Francisco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26136', 'Jardim São Francisco de Assis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26137', 'Jardim São Gabriel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26138', 'Jardim São Gonçalo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26139', 'Jardim São Januário', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26140', 'Jardim São João (São Rafael)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26141', 'Jardim São Joaquim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26142', 'Jardim São Jorge', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26143', 'Jardim São José (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26144', 'Jardim São Judas Tadeu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26145', 'Jardim São Lourenço', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26146', 'Jardim São Luís', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26147', 'Jardim São Manoel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26148', 'Jardim São Martinho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26149', 'Jardim São Mateus', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26150', 'Jardim São Miguel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26151', 'Jardim São Nicolau', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26152', 'Jardim São Paulo(Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26153', 'Jardim São Pedro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26154', 'Jardim São Rafael', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26155', 'Jardim São Remo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26156', 'Jardim São Ricardo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26157', 'Jardim São Roberto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26158', 'Jardim São Roque', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26159', 'Jardim São Savério', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26160', 'Jardim São Sebastião', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26161', 'Jardim São Silvestre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26162', 'Jardim São Vicente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26163', 'Jardim São Vitor', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26164', 'Jardim Sapopemba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26165', 'Jardim Sarah', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26166', 'Jardim Satélite', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26167', 'Jardim Saúde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26168', 'Jardim Scaff', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26169', 'Jardim Seckler', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26170', 'Jardim Selma', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26171', 'Jardim Senice', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26172', 'Jardim Sertãozinho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26173', 'Jardim Sete de Setembro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26174', 'Jardim Shangrilá (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26175', 'Jardim Silva Teles', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26176', 'Jardim Silveira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26177', 'Jardim Sílvia (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26178', 'Jardim Soares', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26179', 'Jardim Solange', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26180', 'Jardim Somara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26181', 'Jardim Sônia (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26182', 'Jardim Sônia Inga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26183', 'Jardim Sonia Marly', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26184', 'Jardim Sônia Regina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26185', 'Jardim Soraia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26186', 'Jardim Souza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26187', 'Jardim Sul São Paulo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26188', 'Jardim Suzana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26189', 'Jardim Sydney', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26190', 'Jardim Taboão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26191', 'Jardim Taipas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26192', 'Jardim Tamoio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26193', 'Jardim Tanay', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26194', 'Jardim Tango', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26195', 'Jardim Tapera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26196', 'Jardim Taquaral', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26197', 'Jardim Teixeira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26198', 'Jardim Tenani', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26199', 'Jardim Teresa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26200', 'Jardim Teresa Cristina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26201', 'Jardim Textil', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26202', 'Jardim Thealia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26203', 'Jardim Thomaz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26204', 'Jardim Tietê', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26205', 'Jardim Tiro ao Pombo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26206', 'Jardim Toca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26207', 'Jardim Tremembé', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26208', 'Jardim Três Corações', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26209', 'Jardim Três Marias', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26210', 'Jardim Triana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26211', 'Jardim Tricolor', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26212', 'Jardim Tropical', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26213', 'Jardim Trussardi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26214', 'Jardim Tuã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26215', 'Jardim Tupã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26216', 'Jardim Tupi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26217', 'Jardim Turquesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26218', 'Jardim Uberaba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26219', 'Jardim Ubirajara (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26220', 'Jardim Uirapuru', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26221', 'Jardim Umarizal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26222', 'Jardim Umuarama', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26223', 'Jardim União', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26224', 'Jardim Universal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26225', 'Jardim Universidade Pinheiros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26226', 'Jardim Universitário', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26227', 'Jardim Vale das Virtudes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26228', 'Jardim Vale do Ribeira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26229', 'Jardim Valquiria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26230', 'Jardim Varginha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26231', 'Jardim Vaz de Lima', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26232', 'Jardim Vazani', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26233', 'Jardim Vera Cruz(Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26234', 'Jardim Vergueiro (Sacomã)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26235', 'Jardim Verônia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26236', 'Jardim Viana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26237', 'Jardim Vieira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26238', 'Jardim Vila Carrão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26239', 'Jardim Vila Formosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26240', 'Jardim Vila Mariana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26241', 'Jardim Vila Nova Progresso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26242', 'Jardim Vilas Boas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26243', 'Jardim Vilma', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26244', 'Jardim Virginia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26245', 'Jardim Virginia Bianca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26246', 'Jardim Vista Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26247', 'Jardim Vista Linda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26248', 'Jardim Vitória Régia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26249', 'Jardim Vivan', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26250', 'Jardim Wanda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26251', 'Jardim Wilma Flor', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26252', 'Jardim Yara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26253', 'Jardim Ymay', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26254', 'Jardim Zaira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26255', 'Jardim Zelia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26256', 'Jardim Zilda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26257', 'Jordanópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26258', 'José Bonifacio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26259', 'Jurubatuba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26260', 'Lajeado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26261', 'Lapa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26262', 'Lapa de Baixo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26263', 'Lauzane Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26264', 'Liberdade', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26265', 'Limão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26266', 'Limoeiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26267', 'Loteamento City Jaragua', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26269', 'Luz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26270', 'Mandaqui', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26271', 'Maranhão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26272', 'Mata Fria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26273', 'Miami Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26274', 'Mirandópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26275', 'Moinho Velho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26276', 'Mooca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26277', 'Conjunto Residencial Morada do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26278', 'Morro do Índio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26279', 'Morro dos Ingleses', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26280', 'Morro Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26281', 'Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26282', 'Nossa Senhora do Ó', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26283', 'Nova Piraju', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26284', 'Núcleo Carvalho de Araújo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26285', 'Núcleo Lageado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26286', 'Pacaembu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26287', 'Paineiras do Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26288', 'Parada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26289', 'Cidade Nova América', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26290', 'Parada Inglesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26291', 'Parada XV de Novembro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26292', 'Paraíso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26293', 'Paraíso do Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26294', 'Paraisópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26295', 'Parelheiros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26296', 'Pari', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26297', 'Parque Alto do Rio Bonito', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26298', 'Parque Alves de Lima', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26299', 'Parque Amazonas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26300', 'Parque Amélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26301', 'Parque América', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26302', 'Parque Anhanguera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26303', 'Parque Anhembi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26304', 'Parque Arariba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26305', 'Parque Artur Alvim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26306', 'Parque Atlântico', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26307', 'Parque bairros Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26308', 'Parque Belém', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26309', 'Parque Boa Esperança', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26310', 'Parque Bologne', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26311', 'Parque Boturussu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26312', 'Parque Brasil', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26313', 'Parque Bristol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26314', 'Parque Casa de Pedra', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26315', 'Parque Central', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26316', 'Parque Císper', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26317', 'Parque Claudia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26318', 'Parque Cocaia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26319', 'Parque Colonial', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26320', 'Parque Continental', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26321', 'Parque Cristina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26322', 'Parque Cruzeiro do Sul', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26323', 'Parque da Mooca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26324', 'Parque da Vila Prudente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26325', 'Parque das Árvores', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26326', 'Parque das Cerejeiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26327', 'Parque das Paineiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26328', 'Parque Deizy', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26329', 'Parque do Carmo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26330', 'Parque do Castelo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26331', 'Parque do Engenho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26332', 'Parque do Estado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26333', 'Parque do Lago', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26334', 'Parque do Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26335', 'Parque do Otero', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26336', 'Parque do Terceiro Lago', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26337', 'Parque Dom João Neri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26338', 'Parque Doroteia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26339', 'Parque dos Bancários', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26340', 'Parque dos Príncipes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26341', 'Parque Edu Chaves', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26342', 'Parque Esmeralda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26343', 'Parque Esperança', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26344', 'Parque Fernanda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26345', 'Parque Flamengo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26346', 'Parque Florestal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26347', 'Parque Fongaro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26348', 'Parque Grajaú', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26349', 'Parque Guaianazes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26350', 'Parque Guarani', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26351', 'Parque Ibirapuera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26352', 'Parque Imperial', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26353', 'Parque Independência', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26354', 'Parque Industrial', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26355', 'Parque Industrial Tomas Edson', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26356', 'Parque Ipê', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26357', 'Parque Itaberaba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26358', 'Parque Jabaquara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26359', 'Parque Jardim Mirassol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26360', 'Parque Lagoa Rica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26361', 'Parque Lagoinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26363', 'Parque Ligia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26364', 'Parque Luis Mucciolo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26365', 'Parque Malagoli', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26366', 'Parque Mandaqui', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26367', 'Parque Mandi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26368', 'Parque Maria Alice', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26369', 'Parque Maria Domitila', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26370', 'Parque Maria Helena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26371', 'Parque Maria Luiza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26372', 'Parque Monteiro Soares', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26373', 'Parque Morro Doce', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26374', 'Parque Muciolo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26375', 'Parque Munhoz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26376', 'Parque Nações Unidas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26377', 'Parque Novo Grajaú', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26378', 'Parque Novo Lar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26379', 'Parque Novo Mundo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26380', 'Parque Novo Santo Amaro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26381', 'Parque Paineiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26382', 'Parque Paiolzinho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26383', 'Parque Panamericano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26384', 'Parque Paulistano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26385', 'Parque Penha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26386', 'Parque Pereira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26387', 'Parque Peruche', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26388', 'Parque Petrópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26389', 'Parque Planalto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26390', 'Parque Primavera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26391', 'Parque Ramos Freitas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26392', 'Parque Reboucas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26393', 'Parque Recreio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26394', 'Parque Regina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26395', 'Parque Residencial Cocaia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26396', 'Parque Residencial D\'Abril', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26397', 'Parque Residencial da Lapa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26398', 'Parque Residencial dos Lagos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26399', 'Parque Residencial Julia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26400', 'Parque Residencial Oratorio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26401', 'Parque Residencial Rio Bonito', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26402', 'Parque Rodrigues Alves', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26403', 'Parque Sabara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26404', 'Parque Santa Amélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26405', 'Parque Santa Bárbara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26406', 'Parque Santa Cecília', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26408', 'Parque Santa Madalena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26409', 'Parque Santa Rita', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26410', 'Parque Santana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26411', 'Parque Santo Amaro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26412', 'Parque Santo Antônio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26413', 'Parque Santo Eduardo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26414', 'Parque São Domingos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26415', 'Parque São Joaquim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26416', 'Parque São Jorge', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26417', 'Parque São José', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26418', 'Parque São Lourenço', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26419', 'Parque São Lucas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26420', 'Parque São Luís', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26421', 'Parque São Miguel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26422', 'Parque São Paulo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26423', 'Parque São Rafael', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26424', 'Parque Savoy City', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26425', 'Parque Sevilha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26426', 'Parque Sonia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26427', 'Parque Taipas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26428', 'Parque Tamari', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26429', 'Parque Tietê', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26430', 'Parque Tomas Saraiva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26431', 'Parque Universitário Espírita', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26432', 'Parque Veloso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26433', 'Parque Veredas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26434', 'Parque Vila Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26435', 'Parque Vila Prudente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26436', 'Parque Vitória', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26437', 'Pedreira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26438', 'Penha de França', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26439', 'Perdizes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26440', 'Jardim dos Pereiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26441', 'Perus', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26442', 'Pinheiros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26443', 'Piqueri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26444', 'Pirajussara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26445', 'Piraporinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26446', 'Pirituba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26447', 'Planalto Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26448', 'Polvilho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26449', 'Ponte Pequena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26450', 'Ponte Rasa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26451', 'Praia Azul', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26452', 'Praia da Lagoa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26453', 'Praia do Leblon', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26454', 'Praia Paulistinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26455', 'Praias Paulistanas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26456', 'Promorar Vila Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26457', 'Protendit', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26458', 'Quarta Parada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26459', 'Quinta da Paineira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26461', 'Rancho Geraldo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26462', 'Raposo Tavares', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26463', 'Real Parque', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26464', 'Recanto Ana Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26465', 'Recanto Campo Belo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26466', 'Recanto dos Sonhos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26467', 'Recanto Marisa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26468', 'Recanto Monte Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26469', 'Recanto Paraíso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26470', 'Recanto Santo Antônio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26471', 'Recanto Verde do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26472', 'República', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26474', 'Residencial Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26475', 'Residencial Sol Nascente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26476', 'Residencial Taipas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26477', 'Retiro Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26478', 'Rio Bonito', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26479', 'Rio Pequeno', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26480', 'Riviera Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26481', 'Rolinópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26482', 'Sacomã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26483', 'Santa Amélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26484', 'Santa Cecília', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26485', 'Santa Efigênia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26486', 'Santa Etelvina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26487', 'Santa Inês', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26488', 'Santa Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26489', 'Santa Teresinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26490', 'Santana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26491', 'Santo Amaro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26492', 'São Domingos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26493', 'São João Clímaco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26494', 'São Judas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26495', 'São Lucas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26496', 'São Mateus', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26497', 'São Miguel Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26498', 'São Salvador', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26499', 'Sapopemba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26500', 'Saúde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26501', 'Sete Praias', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26502', 'Siciliano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26503', 'Sítio Areião', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26504', 'Sítio Barrocada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26505', 'Sitio Boa Vista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26506', 'Sítio Botuquara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26507', 'Sítio Búfalo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26508', 'Sítio Canela Branca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26509', 'Sítio Caraguata', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26510', 'Sítio Cocaia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26511', 'Sítio da Figueira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26512', 'Sítio do Mandaqui', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26513', 'Sítio do Morro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26514', 'Sítio do Piqueri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26515', 'Sítio Itaberaba I', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26516', 'Sítio Laredo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26517', 'Sítio Matsumura', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26518', 'Sítio Morro Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26519', 'Sítio Pinheirinho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26520', 'Sitio Ponderosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26521', 'Sítio Represa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26522', 'Sitio Tapera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26523', 'Sítio Vale Verde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26524', 'Socorro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26525', 'Sumaré', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26526', 'Sumarezinho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26527', 'Super Quadra Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26528', 'Taipas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26530', 'Tatuapé', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26531', 'Terceira Divisão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26532', 'Terceira Divisão de Interlagos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26533', 'Tremembé', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26534', 'Três Cruzes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26535', 'Tucuruvi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26536', 'Tuparacoera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26537', 'Umarizal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26539', 'Usina Piratininga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26540', 'Valo Velho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26541', 'Várzea da Barra Funda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26542', 'Várzea de Baixo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26543', 'Veleiros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26544', 'Vila ABC', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26545', 'Vila Acre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26546', 'Vila Adalgisa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26547', 'Vila Ademar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26548', 'Vila Água Funda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26549', 'Vila Agueda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26550', 'Vila Aimoré', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26551', 'Vila Alabama', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26552', 'Vila Alba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26553', 'Vila Albano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26554', 'Vila Albertina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26555', 'Vila Alexandria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26556', 'Vila Almeida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26557', 'Vila Alpina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26558', 'Vila Alteza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26559', 'Vila Alzira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26560', 'Vila Amália (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26561', 'Vila Amélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26562', 'Vila América', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26563', 'Vila Americana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26564', 'Vila Ana Cláudia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26565', 'Vila Ana Rosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26566', 'Vila Anadir', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26567', 'Vila Analia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26569', 'Vila Anastácio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26570', 'Vila Andes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26571', 'Vila Andrade', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26572', 'Vila Angélica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26573', 'Vila Angelina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26574', 'Vila Anglo Brasileira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26575', 'Vila Anhanguera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26576', 'Vila Antonieta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26577', 'Vila Antonina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26578', 'Vila Antônio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26579', 'Vila Antônio dos Santos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26580', 'Vila Aparecida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26581', 'Vila Aparecida Ivone', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26582', 'Vila Apoena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26583', 'Vila Araguaia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26584', 'Vila Arapuã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26585', 'Vila Arcádia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26586', 'Vila Aricanduva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26587', 'Vila Arriete', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26588', 'Vila Arruda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26589', 'Vila Augusto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26590', 'Vila Aurea', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26591', 'Vila Aurora (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26592', 'Vila Azevedo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26593', 'Vila Babilônia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26594', 'Vila Baby', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26595', 'Vila Bancária', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26596', 'Vila Bancária Munhoz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26597', 'Vila Bandeirantes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26599', 'Vila Barbosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26600', 'Vila Barreto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26601', 'Vila Baruel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26602', 'Vila Basileia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26603', 'Vila Bauab', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26604', 'Vila Beatriz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26605', 'Vila Bela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26606', 'Vila Bela Aliança', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26607', 'Vila Bela do Sapopemba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26608', 'Vila Bela Vista (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26609', 'Vila Belo Horizonte', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26610', 'Vila Bertioga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26611', 'Vila Bianca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26612', 'Vila Boaçava', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26613', 'Vila Bom Jardim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26614', 'Vila Bonilha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26615', 'Vila Bonilha Nova', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26616', 'Vila Borges', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26617', 'Vila Botoni', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26618', 'Vila Bozzini', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26619', 'Vila Brasil', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26620', 'Vila Brasilândia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26621', 'Vila Brasília (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26622', 'Vila Brasilina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26623', 'Vila Brasílio Machado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26624', 'Vila Buarque', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26625', 'Vila Buenos Aires', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26627', 'Vila Butantã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26628', 'Vila Cachoeira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26629', 'Vila Cachoeirinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26630', 'Vila Caiçara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26631', 'Vila Caiúba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26632', 'Vila Caju', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26633', 'Vila Califórnia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26634', 'Vila Calu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26635', 'Vila Campanela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26636', 'Vila Campestre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26637', 'Vila Campo Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26638', 'Vila Canero', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26639', 'Vila Capela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26640', 'Vila Caraguatá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26641', 'Vila Carbone', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26642', 'Vila Cardoso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26643', 'Vila Cardoso Franco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26644', 'Vila Carioca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26645', 'Vila Carlos de Campos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26646', 'Vila Carmem', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26647', 'Vila Carmosina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26648', 'Vila Carolina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26649', 'Vila Carrão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26650', 'Vila Castelo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26651', 'Vila Castro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26652', 'Vila Catupia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26653', 'Vila Cavaton', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26654', 'Vila Caxambu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26655', 'Vila Celeste', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26656', 'Vila Centenário', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26657', 'Vila Central', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26658', 'Vila Chabilândia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26659', 'Vila Chalot', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26660', 'Vila Charlote', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26661', 'Vila Chavantes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26662', 'Vila Chica Luisa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26663', 'Vila Chuca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26664', 'Vila Cisper', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26665', 'Vila Clara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26666', 'Vila Clarice', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26667', 'Vila Cláudia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26668', 'Vila Clélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26669', 'Vila Clementino', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26670', 'Vila Cleonice', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26671', 'Vila Comercial', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26672', 'Vila Conceição', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26673', 'Vila Conde do Pinhal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26674', 'Vila Congonhas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26675', 'Vila Constança', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26676', 'Vila Continental', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26677', 'Vila Corberi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26678', 'Vila Cordeiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26679', 'Vila Cosmopolita', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26680', 'Vila Costa Melo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26681', 'Vila Cristália', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26682', 'Vila Cruz das Almas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26683', 'Vila Cruzeiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26684', 'Vila Cruzeiro Novo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26685', 'Vila Cunha Bueno', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26686', 'Vila Curuçá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26688', 'Vila da Paz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26689', 'Vila da Saúde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26690', 'Vila Dalila', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26691', 'Vila Dalva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26692', 'Vila Damaceno', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26693', 'Vila Danubio Azul', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26694', 'Vila Darli', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26695', 'Vila das Belezas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26696', 'Vila das Mercês', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26697', 'Vila Deodoro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26698', 'Vila Dinorah', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26699', 'Vila Dionisia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26700', 'Vila Diva (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26701', 'Vila Divina Pastora', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26702', 'Vila do Bosque', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26703', 'Vila do Castelo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26704', 'Vila do Encontro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26705', 'Vila Dom José', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26706', 'Vila Dom Pedro I', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26707', 'Vila Dom Pedro II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26708', 'Vila Domitila', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26709', 'Vila Dona Augusta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26710', 'Vila Dona Meta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26711', 'Vila Dona Sara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26712', 'Vila Dorna', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26713', 'Vila dos Andradas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26714', 'Vila dos Andrades', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26715', 'Vila dos Minérios', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26716', 'Vila dos Palmares', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26717', 'Vila dos Remédios', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26718', 'Vila Doutor Eiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26719', 'Vila Duarte', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26720', 'Vila Ede', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26721', 'Vila Elba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26722', 'Vila Eliana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26723', 'Vila Elias Nigri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26724', 'Vila Elida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26725', 'Vila Eliete', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26726', 'Vila Elisio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26727', 'Vila Elvira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26728', 'Vila Elze', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26729', 'Vila Ema', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26730', 'Vila Emir', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26731', 'Vila Erna', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26732', 'Vila Ernesto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26733', 'Vila Escolar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26734', 'Vila Esmeralda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26735', 'Vila Espanhola', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26736', 'Vila Esperança', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26737', 'Vila Ester (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26738', 'Vila Euthalia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26739', 'Vila Fachini', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26740', 'Vila Fanton', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26741', 'Vila Fátima', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26742', 'Vila Fazzeoni', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26743', 'Vila Feliz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26744', 'Vila Fernandes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26745', 'Vila Fiat Lux', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26746', 'Vila Fidalgo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26747', 'Vila Fidelis Ribeiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26748', 'Vila Firmiano Pinto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26750', 'Vila Formosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26751', 'Vila Franca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26752', 'Vila Franci', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26753', 'Vila Francos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26754', 'Vila Friburgo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26755', 'Vila Frugoli', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26756', 'Vila Fukuya', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26757', 'Vila Gabriel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26758', 'Vila Gea', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26759', 'Vila Germaine', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26760', 'Vila Germinal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26761', 'Vila Gertrudes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26762', 'Vila Gil', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26763', 'Vila Gilda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26764', 'Vila Giordano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26765', 'Vila Gomes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26766', 'Vila Gomes Cardim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26767', 'Vila Gouveia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26768', 'Vila Graciosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26769', 'Vila Granada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26770', 'Vila Graziela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26771', 'Vila Guaca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26772', 'Vila Guacuri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26773', 'Vila Guaraciaba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26774', 'Vila Guarani(Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26775', 'Vila Guedes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26776', 'Vila Guilherme', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26777', 'Vila Guilhermina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26778', 'Vila Gumercindo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26779', 'Vila Gustavo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26780', 'Vila Hadad', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26781', 'Vila Hamburguesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26782', 'Vila Hebe', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26783', 'Vila Helena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26784', 'Vila Heliópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26785', 'Vila Hermínia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26786', 'Vila Homero', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26787', 'Vila Hungaresa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26788', 'Vila Icarai', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26789', 'Vila Ida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26790', 'Vila Iguaçu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26791', 'Vila Império', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26792', 'Vila Inácio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26793', 'Vila Inah', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26794', 'Vila Independência', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26795', 'Vila Indiana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26796', 'Vila Industrial', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26797', 'Vila Inglesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26798', 'Vila Invernada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26799', 'Vila Iório', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26800', 'Vila Ipojuca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26801', 'Vila Iracema', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26802', 'Vila Irene', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26803', 'Vila Irmãos Arnoni', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26804', 'Vila Isa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26805', 'Vila Isabel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26807', 'Vila Isolina Mazzei', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26808', 'Vila Itaberaba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26809', 'Vila Itaim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26810', 'Vila Ivg', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26811', 'Vila Ivone', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26812', 'Vila Jacuí', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26813', 'Vila Jaguara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26814', 'Vila Jaguari', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26815', 'Vila Jaraguá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26817', 'Vila Joaniza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26818', 'Vila João Batista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26819', 'Vila Judith', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26820', 'Vila Julio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26821', 'Vila Julio Cesar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26822', 'Vila Jurema', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26823', 'Vila Jussara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26824', 'Vila Klaunig', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26825', 'Vila Lageado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26826', 'Vila Laís', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26827', 'Vila Lar Nacional', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26828', 'Vila Leme', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26829', 'Vila Leonor', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26830', 'Vila Leopoldina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26831', 'Vila Libanesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26832', 'Vila Lisboa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26833', 'Vila Liviero', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26834', 'Vila Londrina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26835', 'Vila Lourdes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26836', 'Vila Lúcia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26837', 'Vila Lúcia Elvira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26838', 'Vila Macedópolis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26839', 'Vila Madalena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26840', 'Vila Mafra', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26841', 'Vila Malvina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26842', 'Vila Mangalot', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26843', 'Vila Mara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26844', 'Vila Maracanã', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26845', 'Vila Marari', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26846', 'Vila Marcelo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26847', 'Vila Marconi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26848', 'Vila Margareth', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26849', 'Vila Margarida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26850', 'Vila Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26851', 'Vila Maria Alta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26852', 'Jardim Maria Augusta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26853', 'Vila Maria Baixa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26854', 'Vila Maria Eugenia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26855', 'Vila Maria Luisa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26856', 'Vila Maria Trindade', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26857', 'Vila Maria Zélia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26858', 'Vila Mariana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26859', 'Vila Marieta', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26860', 'Vila Marilena', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26861', 'Vila Marina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26862', 'Vila Maringá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26863', 'Vila Mariza Mazzei', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26864', 'Vila Marte', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26865', 'Vila Mascote', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26866', 'Vila Matias', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26867', 'Vila Matilde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26868', 'Vila Mazzei', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26869', 'Vila Medeiros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26870', 'Vila Mendes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26871', 'Vila Mercedes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26872', 'Vila Mesquita', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26873', 'Vila Miami', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26874', 'Vila Michelina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26875', 'Vila Minerva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26876', 'Vila Mira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26877', 'Vila Mirante', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26878', 'Vila Miriam', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26879', 'Vila Mirim', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26880', 'Vila Missionária', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26881', 'Vila Moderna', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26882', 'Vila Moinho Velho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26883', 'Vila Monte Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26884', 'Vila Monte Santo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26885', 'Vila Monumento', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26886', 'Vila Moraes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26887', 'Vila Moreira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26888', 'Vila Morgadouro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26889', 'Vila Morro Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26890', 'Vila Morse', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26891', 'Vila Morumbi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26892', 'Vila Nair', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26893', 'Vila Nancy', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26894', 'Vila Nascente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26895', 'Vila Natal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26896', 'Vila Natália', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26897', 'Vila Neila', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26898', 'Vila Nelson', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26899', 'Vila Nhocune', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26900', 'Vila Nilo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26901', 'Vila Nilva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26902', 'Vila Nina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26903', 'Vila Nitro Operária', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26904', 'Vila Nitro Química', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26905', 'Vila Nivi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26906', 'Vila Noca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26907', 'Vila Nogueira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26908', 'Vila Norma', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26909', 'Vila Nossa Senhora Aparecida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26910', 'Vila Nossa Senhora da Conceição', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26912', 'Vila Nossa Senhora do Retiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26913', 'Vila Nova', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26914', 'Vila Nova Alba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26915', 'Vila Nova Cachoeirinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26916', 'Vila Nova Caledônia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26917', 'Vila Nova Carolina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26918', 'Vila Nova Conceição', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26919', 'Vila Nova Curuçá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26920', 'Vila Nova das Belezas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26921', 'Vila Nova Galvão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26922', 'Vila Nova Jaraguá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26923', 'Vila Nova Manchester', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26924', 'Vila Nova Mazzei', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26925', 'Vila Nova Pauliceia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26926', 'Vila Nova Perus', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26927', 'Vila Nova Pirajussara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26928', 'Vila Nova São Miguel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26929', 'Vila Nova Savoia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26930', 'Vila Nova Teresa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26931', 'União de Vila Nova', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26932', 'Vila Nova Utinga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26933', 'Vila Nova York', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26934', 'Vila Odete', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26935', 'Vila Olga', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26936', 'Vila Olímpia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26937', 'Vila Olinda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26938', 'Vila Oratório', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26939', 'Vila Paiva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26940', 'Vila Palmeiras', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26941', 'Vila Paranaguá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26942', 'Vila Parque Jabaquara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26943', 'Vila Patrimonial', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26944', 'Vila Paulicéia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26945', 'Vila Paulina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26946', 'Vila Paulista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26947', 'Vila Paulistana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26948', 'Vila Paulistania', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26949', 'Vila Paulo Silas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26950', 'Vila Pedra Branca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26951', 'Vila Pedroso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26952', 'Vila Penteado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26953', 'Vila Pereira Barreto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26954', 'Vila Pereira Cerca', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26955', 'Vila Perus', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26956', 'Vila Piauí', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26957', 'Vila Picinin', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26958', 'Vila Pierina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26959', 'Vila Piracicaba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26960', 'Vila Pirajussara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26961', 'Vila Pirituba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26962', 'Vila Pita', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26963', 'Vila Plana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26964', 'Vila Polopoli', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26965', 'Vila Pompéia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26966', 'Vila Ponte Rasa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26967', 'Vila Popular', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26968', 'Vila Portugal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26969', 'Vila Portuguesa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26970', 'Vila Prado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26971', 'Vila Praia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26972', 'Vila Prel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26973', 'Vila Primavera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26974', 'Vila Primeiro de Outubro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26975', 'Vila Princesa Isabel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26976', 'Vila Progredior', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26977', 'Vila Progresso (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26978', 'Vila Prudente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26979', 'Vila Quarto Centenário', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26980', 'Vila Quintana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26981', 'Vila Ramos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26982', 'Vila Rancho Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26983', 'Vila Raquel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26984', 'Vila Ré', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26985', 'Vila Regente Feijó', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26986', 'Vila Regina ( Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26987', 'Vila Reis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26988', 'Vila Remo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26989', 'Vila Renato (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26990', 'Vila Represa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26991', 'Vila Ribeiro de Barros', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26992', 'Vila Rica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26993', 'Vila Rio Branco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26994', 'Vila Robertina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26995', 'Vila Rodrigues', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26996', 'Vila Romana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26997', 'Vila Romano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26998', 'Vila Romero', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('26999', 'Vila Roque', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27000', 'Vila Rosa Molla', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27001', 'Vila Rosaria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27002', 'Vila Roschel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27003', 'Vila Rosina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27004', 'Vila Rubi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27005', 'Vila Rui Barbosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27006', 'Vila Sabrina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27007', 'Vila Salete', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27008', 'Vila Sampaio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27009', 'Vila Santa Catarina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27010', 'Vila Santa Clara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27011', 'Vila Santa Cruz (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27012', 'Vila Santa Delfina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27013', 'Vila Santa Edwiges', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27014', 'Vila Santa Eulalia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27015', 'Vila Santa Inês', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27016', 'Vila Santa Isabel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27017', 'Vila Santa Lúcia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27018', 'Vila Santa Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27019', 'Vila Santa Teresa (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27020', 'Vila Santa Teresinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27021', 'Vila Santa Terezinha (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27022', 'Vila Santa Virginia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27023', 'Vila Santana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27024', 'Vila Santista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27025', 'Vila Santo Antônio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27026', 'Vila Santo Estéfano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27027', 'Vila Santo Estevam Reis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27028', 'Vila Santo Estevão', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27029', 'Vila Santo Henrique', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27030', 'Vila Santos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27031', 'Vila São Camilo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27032', 'Vila São Domingos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27033', 'Vila São Francisco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27034', 'Vila São Geraldo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27035', 'Vila São José', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27036', 'Vila São Judas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27037', 'Vila São Luís(Zona Oeste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27038', 'Vila São Nicolau', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27039', 'Vila São Paulo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27040', 'Vila São Pedro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27041', 'Vila São Sebastião', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27042', 'Vila São Silvestre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27043', 'Vila São Vicente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27044', 'Vila Sapopemba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27045', 'Vila Sara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27046', 'Vila Savoy', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27047', 'Vila Seabra', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27048', 'Vila Sérgio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27049', 'Vila Serralheiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27050', 'Vila Silva Teles', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27051', 'Vila Silveira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27052', 'Vila Sílvia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27053', 'Vila Simone', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27054', 'Vila Sinhá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27055', 'Vila Siqueira (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27056', 'Vila Sirene', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27057', 'Vila Siria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27058', 'Vila Socorro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27059', 'Vila Sofia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27060', 'Vila Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27061', 'Vila Solange', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27062', 'Vila Sônia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27063', 'Vila Souza', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27064', 'Vila Stela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27065', 'Vila Suiça', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27066', 'Vila Sulina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27067', 'Vila Suzana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27068', 'Vila Taiau', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27069', 'Vila Talarico', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27070', 'Vila Taquari', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27071', 'Vila Teresinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27072', 'Vila Tiradentes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27073', 'Vila Tolstoi', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27074', 'Vila Tramontano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27075', 'Vila Uberabinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27076', 'Vila União (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27077', 'Vila Universitária', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27078', 'Vila Ursulina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27079', 'Vila Valdemar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27080', 'Vila Vasconcelos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27081', 'Vila Vera', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27082', 'Vila Verde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27083', 'Vila Vermelha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27084', 'Vila Vessoni', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27085', 'Vila Virginia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27087', 'Vila Vitório Mazzei', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27088', 'Vila Yara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27089', 'Vila Iolanda(Lajeado)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27090', 'Vila Zat', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27091', 'Vila Zefira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27092', 'Vila Zelina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27093', 'Vila Zilda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27094', 'Vila Zulmira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('27095', 'Vista Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('29208', 'Jardim Bárbara', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('29288', 'Jardim Jaú (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('29400', 'Sé', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('29401', 'Aclimação', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('29404', 'Vila Rosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('29406', 'Vila Luzimar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('29409', 'Vila Iolanda II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('29486', 'Jardim Sabiá II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('29922', 'Alto da Boa Vista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('29977', 'Parque das Flores', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30134', 'Jardim São Gilberto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30353', 'Jardins', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30414', 'Sítio Itaberaba II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30415', 'Jardim Rosinha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30417', 'Vila Guiomar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30424', 'Vila Jaci', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30425', 'Jardim Pérola II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30426', 'Vila Roseira II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30427', 'Jardim Pedra Branca II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30428', 'Vila Paulista I', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30429', 'Barro Branco II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30473', 'Conjunto Residencial Sabará - Campo Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30500', 'Palanque', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30501', 'Vila Independente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30509', 'Jardim Filhos da Terra', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30550', 'Conjunto City Jaraguá', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30553', 'Jardim Pérola III', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30556', 'Jardim Pérola I', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30557', 'Jardim Vitória', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30558', 'Jardim São Raimundo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30569', 'Associação Sobradinho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30571', 'Jardim Recanto Verde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30572', 'Jardim Felicidade (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30573', 'Jardim Fontalis', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30574', 'Jardim Palmares (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30580', 'Condomínio São Marcos', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30617', 'Jardins Recanto das Rosas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30623', 'Jardim Danubio Azul', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30664', 'Cohab Monet', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30768', 'Jardim Poleti', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30779', 'Jardim Sandra Maria', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30780', 'Jardim Pantanal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30781', 'Vila Nova Parada', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30824', 'Cantinho do Céu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30826', 'Chácara Bambú', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30913', 'Jardim Fanganiello', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30914', 'Jardim Áurea', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30921', 'Parque Palmas do Tremembé', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30924', 'Colônia (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30949', 'Jardim Vitória Régia (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30977', 'Jardim São Luís (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('30979', 'Vila Ester', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('31041', 'Jardim São Carlos (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('31044', 'Jardim das Palmeiras (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('31045', 'Jardim São Luís (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('31046', 'Vila Vanda', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('31060', 'Vila Bela Vista (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('31117', 'Jardim Imperador (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('32128', 'Chácara Santo Antônio (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('32431', 'Jardim Santa Terezinha (Parelheiros)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('32863', 'Jardim Santa Fé (Zona Oeste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34008', 'Conjunto Habitacional Turística', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34100', 'Jardim Shangrilá (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34101', 'Jardim São Pedro (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34110', 'Jardim Sipramar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34111', 'Condomínio Jequirituba', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34112', 'Jardim Novo Jaú', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34149', 'Jardim Alvorada (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34150', 'Jardim Alvorada (Zona Oeste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34179', 'Jardim Ipanema (Zona Oeste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34180', 'Jardim Ipanema (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34211', 'Jardim Aurora (Zona Oeste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34311', 'Jardim das Laranjeiras (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34316', 'Vila Ayrosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34328', 'Jardim das Rosas (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34329', 'Jardim das Rosas (Zona Leste I)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34332', 'Jardim das Rosas (Iguatemi)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34344', 'Vila Narciso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34346', 'Jardim Prainha', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34348', 'Jardim Monte Verde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34479', 'Vila Pedrosa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34568', 'Jardim Paulistano (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34756', 'Jardim São João (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34775', 'Jardim Portal I e II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34776', 'Jardim Hebrom', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34777', 'Jardim Apuana', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34778', 'Jardim Campo Limpo (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34779', 'Jardim Flor de Maio', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34780', 'Jardim da Cachoeira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34781', 'Jardim Estrela D\'alva', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34782', 'Jardim Uniserve', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34783', 'Jardim Corisco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34784', 'Jardim Francisco Mendes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34785', 'Jardim Vila Rica', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34786', 'Jardim Labitary', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34787', 'Núcleo do Engordador', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34788', 'Jardim Brasil Novo', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34789', 'Jardim Denise', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34952', 'Jardim Sílvia (Zona Oeste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('34958', 'Vila Amália (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('35176', 'Jardim Bela Vista (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36085', 'Vila Siqueira (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36183', 'Vila Progresso (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36184', 'Vila Progresso (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36237', 'Jardim da Laranjeira (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36239', 'Jardim Bandeirante (São Rafael)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36240', 'Jardim Maria Lídia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36274', 'Jardim Limoeiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36294', 'Jardim Alto Alegre (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36339', 'Jardim São João (Guaianazes)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36401', 'Jardim São João', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36402', 'Vila do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36403', 'Jardim São João (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36405', 'Jardim Bandeirantes (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('36406', 'Jardim São Francisco (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('37264', 'Chácara do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('37482', 'Recanto Frei Damião', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('37657', 'Jardim Cidália', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('37658', 'Cupecê', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('37676', 'Jardim Cupecê', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('37977', 'Vila Guarani(Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('37982', 'Jardim Angelina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('38026', 'Jardim Almeida', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('38572', 'Jardim Brasil (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('39565', 'Jardim Arantes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('39566', 'Jardim Vera Cruz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('39567', 'Jardim Vera Cruz(Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('39568', 'Jardim São Francisco(Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('39570', 'Chácaras Lago Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('39571', 'Lagoa Grande', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('40301', 'Jardim Dinar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('40430', 'Fazenda Caguaçu', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('40470', 'Jardim do Colégio (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('40866', 'Jardim Marabá(Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('40871', 'Vila Califórnia(Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('41188', 'Vila Santa Teresa (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('41219', 'Jardim São Paulo(Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('41220', 'Jardim das Camélias (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('41559', 'Jardim das Pedras(Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('41760', 'Jardim Azano I', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('41761', 'Jardim Azano II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('41820', 'Vila União(Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('41835', 'Vila Diva (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('42771', 'Jardim São João (Jaraguá)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('43157', 'Cidade Luz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('43280', 'Jardim Boa Vista (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('43457', 'Vila Renato (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('43859', 'Jardim Santa Terezinha (Pedreira)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('43874', 'Jardim das Oliveiras (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('43991', 'Jardim Novo Lar', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('43996', 'Jardim Beatriz (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('43997', 'Vila Arco Íris', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('43998', 'Jardim Vergueiro', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('43999', 'Jardim Myrna II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('44003', 'Jardim Itapema', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('44017', 'Jardim Brasília (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('44018', 'Jardim Brasília', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('44035', 'Jardim Ipanema (Cidade Líder)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('44222', 'Jardim Lourdes (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('44235', 'Chácara do Conde', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('45006', 'Jardim Vera Cruz (Parelheiros)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('45867', 'Parque Cruzeiro do Sul (Vila Formosa)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('45874', 'Jardim Roschel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('45878', 'Jardim Santa Cruz (Campo Grande)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('45879', 'Jardim Ubirajara (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('45894', 'Parque Santa Cecília (Grajaú)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('45953', 'Jardim Santa Cruz (Sacomã)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('45957', 'Jardim Oriental (Parelheiros)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('45961', 'Vila São Silvestre (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48108', 'Jardim Santa Cruz (Parelheiros)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48113', 'Jardim Jaraguá (São Domingos)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48114', 'Parque Anhanguera (São Domingos)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48115', 'Jardim São José (Artur Alvim)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48116', 'Jardim São José (São Mateus)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48117', 'Jardim São José', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48118', 'Vila São José (Ipiranga)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48119', 'Vila São José (Cidade Dutra)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48120', 'Vila São José (Itaim Paulista)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48121', 'Vila São José (Guaianazes)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48125', 'Jardim Santo Elias (São Miguel)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48468', 'Jardim Jaraguá (Itaim Paulista)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48485', 'Jardim das Fontes', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48518', 'Vila Santa Cruz', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48550', 'Vila Aurora', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48551', 'Jardim Panorama (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48552', 'Jardim Panorama (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48553', 'Jardim Esmeralda (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48597', 'Vila Bruna', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48751', 'Jardim Mar Lune', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48825', 'Jardim Santa Edwiges (Capela do Socorro)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48827', 'Jardim São José (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48828', 'Jardim Castro Alves', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48872', 'Flor da Cantareira', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48880', 'Jardim Paquetá (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48884', 'Jardim da Conquista (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48904', 'Jardim Vale do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48981', 'Jardim Papai Noel', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48994', 'Jardim Aristocrata', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('48995', 'Jardim São Norberto', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49046', 'Jardim Valparaiso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49095', 'Recanto das Andorinhas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49167', 'Conjunto Habitacional Parque Valo Velho II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49191', 'Jardim Monte Alegre (Zona Norte)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49192', 'Jardim Monte Belo (Raposo Tavares)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49233', 'Jardim Represa (Parelheiros)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49234', 'Vila Paulista II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49508', 'Residencial Vilela', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49533', 'Jardim Paraíso', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49624', 'Jardim Alto Paulistano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49626', 'Jardim Nova Conquista', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49661', 'Jardim São Francisco de Assis (Parelheiros)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49759', 'Recanto Alegre', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49761', 'Jardim Nova Vitória I', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49762', 'Jardim Nova Vitória II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49771', 'Jardim Nova Harmonia', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49835', 'Gramado', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('49999', 'Jardim Premiano', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('50248', 'Jardim das Carmelitas', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('50293', 'Jardim Paraná', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('50309', 'Jardim São Jorge (Raposo Tavares)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('50427', 'Jardim Cris', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('50432', 'Vila São Francisco (Zona Sul)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('50433', 'Vila São Francisco (Zona Leste)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('50895', 'Jardim Colibri', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('51199', 'Parque da Lapa', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('51399', 'Vila Hortência', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('52453', 'Cidade de Deus', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('52607', 'Vila Isabel (Parelheiros)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('52824', 'Jardim Ninho Verde II', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('53019', 'Jardim Novo Pantanal', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('53434', 'Jardim Morada do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('53520', 'Jardim Helian', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('53681', 'Moema', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('53748', 'Jardim Chácaras Oriente', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('54014', 'Jardim Camargo Velho', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('54025', 'Vila Nova Esperança', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('54030', 'Conjunto Habitacional Morro da Esperança (Cachoeirinha)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('54050', 'Morada do Sol', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('54213', 'Vila Olga Cecília', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('54241', 'Vila Genioli', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('54362', 'Sítio da Barra', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('54441', 'Vila Regina', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('54702', 'Vila Industrial ( Vila Nova Curuçá)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('54856', 'Parque Santo Antônio (Aricanduva)', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('55014', 'Jardim Cruz do Corisco', 'São Paulo');
INSERT INTO `neighborhoods` VALUES ('55328', 'Jardim Primavera (Zona Sul)', 'São Paulo');

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `total` decimal(11,2) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `restaurant_id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `status` int(11) unsigned NOT NULL,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Pedidos';

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for `order_products`
-- ----------------------------
DROP TABLE IF EXISTS `order_products`;
CREATE TABLE `order_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `order_id` int(11) unsigned DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Produtos de um pedido';

-- ----------------------------
-- Records of order_products
-- ----------------------------

-- ----------------------------
-- Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `price` decimal(10,2) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Produtos';

-- ----------------------------
-- Records of products
-- ----------------------------

-- ----------------------------
-- Table structure for `product_ratings`
-- ----------------------------
DROP TABLE IF EXISTS `product_ratings`;
CREATE TABLE `product_ratings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `likes` int(11) DEFAULT NULL,
  `dislikes` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Qualificação dos produtos';

-- ----------------------------
-- Records of product_ratings
-- ----------------------------

-- ----------------------------
-- Table structure for `restaurants`
-- ----------------------------
DROP TABLE IF EXISTS `restaurants`;
CREATE TABLE `restaurants` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'Dono do restaurante',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Restaurantes';

-- ----------------------------
-- Records of restaurants
-- ----------------------------

-- ----------------------------
-- Table structure for `restaurants_cuisines`
-- ----------------------------
DROP TABLE IF EXISTS `restaurants_cuisines`;
CREATE TABLE `restaurants_cuisines` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) unsigned DEFAULT NULL,
  `cuisine_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tipos de cozinha de um restaurante';

-- ----------------------------
-- Records of restaurants_cuisines
-- ----------------------------

-- ----------------------------
-- Table structure for `restaurants_neighborhoods`
-- ----------------------------
DROP TABLE IF EXISTS `restaurants_neighborhoods`;
CREATE TABLE `restaurants_neighborhoods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) unsigned DEFAULT NULL,
  `neighborhood_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bairros em que os restaurantes atendem';

-- ----------------------------
-- Records of restaurants_neighborhoods
-- ----------------------------

-- ----------------------------
-- Table structure for `restaurants_products`
-- ----------------------------
DROP TABLE IF EXISTS `restaurants_products`;
CREATE TABLE `restaurants_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) unsigned DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Produtos de um restaurante';

-- ----------------------------
-- Records of restaurants_products
-- ----------------------------

-- ----------------------------
-- Table structure for `restaurant_ratings`
-- ----------------------------
DROP TABLE IF EXISTS `restaurant_ratings`;
CREATE TABLE `restaurant_ratings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'Autor do comentário',
  `likes` int(11) unsigned DEFAULT NULL,
  `dislikes` int(11) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Qualificação dos restaurantes';

-- ----------------------------
-- Records of restaurant_ratings
-- ----------------------------

-- ----------------------------
-- Table structure for `restaurant_reviews`
-- ----------------------------
DROP TABLE IF EXISTS `restaurant_reviews`;
CREATE TABLE `restaurant_reviews` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'Autor do comentário',
  `restaurant_id` int(11) unsigned DEFAULT NULL,
  `body` text CHARACTER SET latin1,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Comentários de um restaurantes';

-- ----------------------------
-- Records of restaurant_reviews
-- ----------------------------

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Perfis de Usuários';

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'Usuário', 'usuario');
INSERT INTO `roles` VALUES ('2', 'Admin de Restaurante', 'admin_restaurante');
INSERT INTO `roles` VALUES ('3', 'Admin do Portal', 'admin_portal');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `role_id` int(11) unsigned NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `neighborhood_id` int(11) unsigned DEFAULT NULL,
  `provider` varchar(50) DEFAULT NULL,
  `provider_uid` varchar(50) DEFAULT NULL,
  `hybrid_session` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='Usuários do Site e Admins';

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('3', 'admin', 'admin@admin.com', 'Admin', '3', 'efed97b857c8ef60938a27c324f1986b4d83ee6d', '2012-10-21 22:05:10', '2012-10-21 22:05:10', null, null, null, null, null);
INSERT INTO `users` VALUES ('15', 'Rafael_Santos_Sa', 'rafa@rafael-santos.com', 'Rafael Santos Sá', '1', '0e1fb17ebcb760ef8f84410f041f80293a47cfe2', '2012-11-02 20:34:52', '2012-11-02 20:34:52', null, '25467', 'Facebook', '577126948', null);
INSERT INTO `users` VALUES ('16', 'Riane_Pereira', 'rianepr@gmail.com', 'Riane Pereira', '1', '0e1fb17ebcb760ef8f84410f041f80293a47cfe2', '2012-11-04 21:23:53', '2012-11-04 21:23:53', null, '25467', 'Facebook', '100002241292432', null);
